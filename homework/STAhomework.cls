\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{STAhomewok}[2023/10/17 Homework assigmenents for course 132STA]

\LoadClass[11pt]{article}

%% Required packages
\RequirePackage[a4paper,margin=2cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[numbers]{natbib}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{pdfpages}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{ifthen}
\RequirePackage{xifthen}


%% Bibliography setup
\bibliographystyle{elsarticle-harv}

%% Graphicx setup
\graphicspath{{figures/}}

%% Other commands
\newcommand{\noitemsep}{\itemsep=0pt}

\newcounter{SessionNumber}
\newcounter{ProblemNumber}

\renewcommand{\maketitle}[2]{%
   \setcounter{SessionNumber}{#1}%
    %% Facyhdr setup
    \pagestyle{fancy}%
    \fancyhf{}%
    \chead{Structural Analysis - 132STA | Homework assignment \Roman{SessionNumber}: #2}%
    \rfoot{\thepage}%
}

\newcommand{\ShortTest}[2]{%
    \paragraph{Five-minute problem}%
    \cite[#1]{Hibbeler:2019:SA}
    Determine the diagrams of non-zero internal forces. 
%
\begin{center}
 \includegraphics[width=#2\textwidth]{short_test-\theSessionNumber}%
\end{center}
%
\vspace*{-1em}\hrulefill\vspace*{-1em}
}

\newcommand{\SelfStudy}[1]{%
    \paragraph{Self-study resources}%
    #1
}

\newcommand{\Problem}[2]{%
    \paragraph{Problem}%
    #1%
    \ifthenelse{\isempty{#2}}{}{%
    Image source:~\cite[page~#2]{Hibbeler:2019:SA}}
}

\newcommand{\References}{%
\bibliography{liter.bib}
}

\newcommand{\Request}{%
\paragraph{Request} Plese feel free to submit any errors or recommendations using the issue tracker system at \url{https://gitlab.com/open-mechanics/132sta_structural-analysis/-/issues}\texttt{>New issue} (requires registration) or this \href{incoming+open-mechanics-132sta-structural-analysis-37811058-4unlmwfpk96rfyh3eissxpb0h-issue@incoming.gitlab.com}{email address}.
}
