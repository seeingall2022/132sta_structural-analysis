\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{STA_lectures}[2022/09/18 Course lecture notes]

\LoadClass[11pt]{article}

%% Required packages
\RequirePackage[a4paper,margin=2cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[numbers]{natbib}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{pdfpages}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}

%% Bibliography setup
\bibliographystyle{elsarticle-harv}

%% Graphicx setup
\graphicspath{{figures/}}

%% Other commands
\newcommand{\noitemsep}{\itemsep=0pt}

\newcounter{LectureNumber}

\renewcommand{\maketitle}[2]{%
   \setcounter{LectureNumber}{#1}%
    %% Facyhdr setup
    \pagestyle{fancy}%
    \fancyhf{}%
    \chead{132STA | Lecture~\Roman{LectureNumber}: #2}%
    \rfoot{\thepage}%
}

\newcommand{\References}{%
\bibliography{liter}
}