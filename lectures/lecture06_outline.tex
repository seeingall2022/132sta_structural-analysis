\documentclass{STA_lectures}

\usepackage{amssymb}
\usepackage{pdfpages}

\newcommand{\mtrx}[1]{{\boldsymbol #1}}
\newcommand{\trn}{^\mathsf{T}}
\newcommand{\elem}[1]{^{(#1)}}
\newcommand{\de}{\, \mathrm{d}}


\newcommand{\recall}{\textcolor{gray}{[recall]}}

\begin{document}
    
\maketitle{6}{Direct stiffness method via the principle of virtual work}

\paragraph{Truss structures revisited}

\begin{itemize}\noitemsep

\item \recall~Tonti diagram for 1D elasticity

\begin{itemize}\noitemsep
    \item $x^\ell \in (0, L)$
    \item $u^\ell \rightarrow \varepsilon \rightarrow N \rightarrow f_{x^\ell}$
\end{itemize}

\end{itemize}

\paragraph{Principle of virtual work}

\begin{itemize}

\item $\delta W_\mathrm{int}( \delta u^\ell ) = \delta W_\mathrm{ext}( \delta u^\ell )$ for all admissible $\delta u^\ell$

\item For a truss bar
%
\begin{align}
    \delta W_\mathrm{int}( \delta u^\ell ) 
    =
    \int_0^L
    \delta \varepsilon(x^\ell) N( x^\ell )
    \de x^\ell,
    &&
    \delta W_\mathrm{ext}( \delta u^\ell ) 
    =
    \int_0^L
    \delta u^\ell(x^\ell) f_x^\ell ( x^\ell )
    \de x^\ell,
\end{align}

\item New variant of the Tonti diagram
%
\begin{itemize}\noitemsep
    \item $\displaystyle \int_0^L \delta u^\prime \bullet  = \int_0^L \delta u$
\end{itemize}

\begin{align}
    \int_0^L
    (\delta u^\ell)^\prime (x^\ell) EA ( u^\ell)^\prime( x^\ell )
    \de x^\ell
    =
    \int_0^L
    \delta u^\ell(x^\ell) f_x^\ell ( x^\ell )
    \de x^\ell,
\end{align}

\end{itemize}

\paragraph{Matrix form}

\begin{itemize}

\item Displacement in the from
%
\begin{align}
    u^\ell( x^\ell ) 
    & = 
    \left( 
        1 - \frac{x^\ell}{L}
    \right) u_a^\ell  
    +
    \frac{x^\ell}{L}
    u_b^\ell 
    &&
    \xi = \frac{x^\ell}{L}
    \\
    u^\ell( \xi )
    & = 
    u_a^\ell \left( 
        1 - \xi
    \right) 
    +
    u_b^\ell \cdot \xi
    &&
    \de \xi 
    = 
    \frac{1}{L} \de x^\ell
    \\
    u^\ell( \xi )
    & =
    \begin{bmatrix}
        1 - \xi & \xi 
    \end{bmatrix}
    \begin{bmatrix}
        u_a^\ell \\ u_b^\ell
    \end{bmatrix}
    = 
    \mtrx{N}^\ell( \xi ) \,
    \mtrx{d}^\ell
\end{align}

\item Strain fields
%
\begin{align}
    \varepsilon( \xi )
    =
    \frac{\de u^\ell}{\de \xi}( \xi ) \frac{\de \xi}{\de x^\ell}( \xi )
    =
    \frac{1}{L} 
    \begin{bmatrix}
    -1 & 1     
    \end{bmatrix}
    \begin{bmatrix}
        u_a^\ell \\ u_b^\ell
    \end{bmatrix}
    = 
    \mtrx{B}^\ell (\xi) \,
    \mtrx{d}^\ell
\end{align}

\item Virtual fields
%
\begin{align}
    \delta u^\ell( \xi )
    & =
    \begin{bmatrix}
        1 - \xi & \xi 
    \end{bmatrix}
    \begin{bmatrix}
        \delta u_a^\ell \\ \delta u_b^\ell
    \end{bmatrix}
    = 
    \mtrx{N}^\ell( \xi ) \,
    \delta \mtrx{d}^\ell,
    &&
    \delta \varepsilon( \xi )
    =
    \frac{1}{L} 
    \begin{bmatrix}
    -1 & 1     
    \end{bmatrix}
    \begin{bmatrix}
        \delta u_a^\ell \\ \delta u_b^\ell
    \end{bmatrix}
    = 
    \mtrx{B}^\ell (\xi) \,
    \delta \mtrx{d}^\ell
    \\    
\end{align}

\end{itemize}

\paragraph{Stiffness matrix}

\begin{itemize}

\item Virtual work of internal forces
%
\begin{align}
    \delta W_\mathrm{int}( \delta \mtrx{d}^\ell ) 
    & =
    \int_0^L
    \delta \varepsilon(x^\ell) N( x^\ell )
    \de x^\ell
    =
    \int_0^1 
    \left( \mtrx{B}^\ell (\xi) \,
    \delta \mtrx{d}^\ell \right)\trn    
    EA \,
    \mtrx{B}^\ell (\xi) \,
    \mtrx{d}^\ell
    L \de \xi    
    \\
    & =
    ( \delta \mtrx{d}^\ell )\trn
    L 
    \int_0^1 
    \mtrx{B}^\ell (\xi)\trn \,
    EA \,
    \mtrx{B}^\ell (\xi) \,
    \de \xi \,
    \mtrx{d}^\ell
    =
    ( \delta \mtrx{d}^\ell )\trn
    \mtrx{K}^\ell
    \mtrx{d}^\ell
\end{align}

\item Stiffness matrix
%
\begin{align}
    \mtrx{K}^\ell 
    =
    \frac{L}{L^2} 
    \int_0^1 
    \begin{bmatrix}
        -1 \\ 1
    \end{bmatrix}
    EA \,
    \begin{bmatrix}
        -1 & 1
    \end{bmatrix}
    \de \xi \,
    =
    \frac{EA}{L}
    \begin{bmatrix}
        1 & -1 \\
       -1 &  1
    \end{bmatrix}
\end{align}

\end{itemize}

\paragraph{Matrix of equivalent nodal forces}

\begin{itemize}\noitemsep
    \item Virtual work of external forces
    %
    \begin{align}
        \delta W_\mathrm{ext}( \delta \mtrx{d}^\ell ) 
        & =
        \int_0^L
        \delta u^\ell(x^\ell) f_x^\ell ( x^\ell )
        \de x^\ell
        =
        \int_0^1 \left( 
            \mtrx{N}^\ell( \xi ) \,
            \delta \mtrx{d}^\ell
        \right)\trn
        f_x^\ell ( x^\ell )
        L \de \xi \\
        & =
        ( \delta \mtrx{d}^\ell )\trn
        L \int_0^1
        \mtrx{N}^\ell( \xi )\trn 
        f_{x^\ell} ( \xi )
        \de \xi
        =
        ( \delta \mtrx{d}^\ell )\trn
        \mtrx{f}^\ell
    \end{align}

    \item Matrix of equivalent nodal forces~(determined using MATLAB)
    %
    \begin{align}
        \mtrx{f}^\ell
        =
        L
        \int_0^1 
        \begin{bmatrix}
            1 - \xi \\
            \xi 
        \end{bmatrix}
        \, 
        \xi \mathrm{f}
        \de \xi
        = 
        \frac{\mathrm{f} L}{3} 
        \begin{bmatrix}
            1 \\ 2
        \end{bmatrix}
    \end{align}
\end{itemize}

\paragraph{Tonti diagram}

\begin{itemize}
    \item $\mtrx{d}^\ell \rightarrow \varepsilon \rightarrow N \rightarrow f_{x^\ell}$
    \item $\displaystyle \delta \mtrx{d}\trn \int_0^L \mtrx{B}\trn \bullet  = \delta \mtrx{d}\trn \int_0^L \mtrx{N}\trn$

\end{itemize}

\paragraph{Global matrices}

\begin{itemize}\noitemsep

\item Coordinate transformation

\begin{align}
    \begin{bmatrix}
        u_a^\ell \\ u_b^\ell
    \end{bmatrix}
    =
    \begin{bmatrix}
        c & s & 0 & 0 & 0 & 0 \\
        0 & 0 & 0 & c & s & 0 \\
    \end{bmatrix}
    \begin{bmatrix}
        u_a \\ w_a \\ \varphi_a \\
        u_b \\ w_b \\ \varphi_b 
    \end{bmatrix}, &&
    \mtrx{d}^\ell = \mtrx{T} \mtrx{d}, &&
    \delta \mtrx{d}^\ell = \mtrx{T} \delta \mtrx{d}
\end{align}

\item Virtual works of internal and external forces
%
\begin{align}
    \delta W_\mathrm{int}( \delta \mtrx{d}^\ell ) 
    & =
    ( \delta \mtrx{d}^\ell )\trn
    \mtrx{K}^\ell
    \mtrx{d}^\ell
    =
    ( \mtrx{T} \delta \mtrx{d} )\trn
    \mtrx{K}^\ell
    \mtrx{T} \delta \mtrx{d} 
    =
    \delta\mtrx{d}\trn \mtrx{T}\trn
    \mtrx{K}^\ell
    \mtrx{T} \delta \mtrx{d} 
    =
    \mtrx{d}\trn \mtrx{K}_\mathrm{m} \mtrx{d}
    =
    \delta W_\mathrm{int}( \delta \mtrx{d} )
    \\
    \delta W_\mathrm{ext}( \delta \mtrx{d}^\ell )
    & =
    ( \delta \mtrx{d}^\ell )\trn \mtrx{f}^\ell
    =
    ( \mtrx{T} \delta \mtrx{d} )\trn \mtrx{f}^\ell
    =
    \delta \mtrx{d}\trn \mtrx{T}\trn \mtrx{f}^\ell
    = 
    \delta \mtrx{d}\trn \mtrx{f}_\mathrm{m}
\end{align}

\item Corresponds exactly to the procedure given in~\cite{Hibbeler:2019:SA}

\end{itemize}

\paragraph{Beam structures revisited}

\begin{itemize}\noitemsep
    \item \recall~Tonti diagram 
    
    \item Principle of virtual work
    %
    \begin{align}
        \int_0^L 
        \delta \kappa( x^\ell ) M( x^\ell )
        \de x^\ell
        =
        \int_0^L
        \delta w^\ell( x^\ell ) f_z^\ell( x^\ell )
        \de x^\ell
    \end{align}

\end{itemize}

\paragraph{Kinematics}

\begin{itemize}\noitemsep
    \item Expressed conveniently with dimensionless coordinate $\xi$
    %
    \begin{align*}
        w^\ell( \xi ) 
        & \approx
        ( 1 - 3 \xi^2 + 2 \xi^3 )
        w_a^\ell
        +
        L ( -\xi + 2 \xi^2 - \xi^3 )
        \varphi_a  
        +
        ( 3 \xi^2 - 2 \xi^3 )
        w_b^\ell 
        +
        L ( \xi^2 - \xi^3 )
        \varphi_b
        \\
        & = 
        \begin{bmatrix}
            ( 1 - 3 \xi^2 + 2 \xi^3 ) &
            L ( -\xi + 2 \xi^2 - \xi^3 ) & 
            ( 3 \xi^2 - 2 \xi^3 ) & 
            L ( \xi^2 - \xi^3 )
        \end{bmatrix}  
        %
        \begin{bmatrix}
        w_a^\ell \\ \varphi_a \\ w_b^\ell \\ \varphi_b    
        \end{bmatrix}  
        \\
        & = \mtrx{N}^\ell( \xi ) \mtrx{d}^\ell
    \end{align*}

    \item Curvature
    %
    \begin{align*}
        \kappa( \xi ) 
        & = 
        - w^\ell( \xi )^{\prime \prime}
        \approx 
        \frac{1}{L^2} 
        \begin{bmatrix}
            ( 6 - 12 \xi ) &
            L ( 6 \xi - 4 ) & 
            ( 12 \xi - 6  ) & 
            L ( 6 \xi - 2 )
        \end{bmatrix}  
        %
        \begin{bmatrix}
        w_a^\ell \\ \varphi_a \\ w_b^\ell \\ \varphi_b    
        \end{bmatrix} \\
        & = \mtrx{B}(\xi) \mtrx{d}^\ell  
    \end{align*}
\end{itemize}

\paragraph{Matrices~(determined using MATLAB)}

\begin{itemize}\noitemsep
    \item Stiffness matrix 
    %
    \begin{align*}
        \mtrx{K}^\ell 
        =
        L \int_0^1 \mtrx{B}(\xi)\trn EI \mtrx{B}(\xi) \de \xi
        =
        \frac{2EI}{L}
        \begin{bmatrix}
           6 / L^2 & -3 / L & -6 / L^2 & -3 / L \\
          -3 / L   &      2 & 3c / L   & 1 \\
          -6 / L^2 &  3 / L &  6 / L^2 &  3 / L \\ 
          -3 / L   &  1     &  3 / L   & 2 
       \end{bmatrix}    
    \end{align*}

    \item Matrix of equivalent nodal forces
    %
    \begin{align*}
        \mtrx{f}^\ell
        =
        L \int_0^1 
        \mtrx{N}(\xi) \cdot \mathrm{f} \xi 
        \de \xi
        =
        \mathrm{f} L 
        \begin{bmatrix}
             3 / 20 \\
            -L / 30 \\
             7 / 20 \\
             L / 20
        \end{bmatrix} 
    \end{align*}

    \item Transformation matrix
    %
    \begin{align}
        \begin{bmatrix}
            w_a^\ell \\ 
            \varphi_a \\ 
            w_b^\ell \\ 
            \varphi_b
        \end{bmatrix}
        =
        \begin{bmatrix}
           -s & c & 0 & 0 & 0 & 0 \\
            0 & 0 & 1 & 0 & 0 & 0 \\ 
            0 & 0 & 0 & -s & c & 0 \\
            0 & 0 & 0 & 0 & 0 & 1 
        \end{bmatrix}
        \begin{bmatrix}
            u_a \\ w_a \\ \varphi_a \\
            u_b \\ w_b \\ \varphi_b 
        \end{bmatrix}
    \end{align}
    
    \item Element matrices
    %
    \begin{align*}
        \mtrx{K}_\mathrm{b}\elem{e}
        =
        \mtrx{T}\trn \mtrx{K}^\ell \mtrx{T}, 
        &&
        \mtrx{f}_\mathrm{b}
        =
        \mtrx{T}\trn \mtrx{f}^\ell
    \end{align*}

    \item Reproduces the previous lectures

\end{itemize}

\References

\includepdf[pages=1-]{codes/lecture_06.pdf}

\end{document}