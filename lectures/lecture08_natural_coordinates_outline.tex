\documentclass{STA_lectures}

\usepackage{amssymb}
\usepackage{pdfpages}

\newcommand{\mtrx}[1]{{\boldsymbol #1}}
\newcommand{\trn}{^\mathsf{T}}
\newcommand{\elem}[1]{^{(#1)}}
\newcommand{\de}{\, \mathrm{d}}


\newcommand{\recall}{\textcolor{gray}{[recall]}}

\begin{document}
    
\maketitle{8}{2D elasticity: Finite Element Method}

\section{Principle of virtual work}

\begin{itemize}\noitemsep

    \item Virtual work done by external forces~(body forces $\overline{\mtrx{X}}$ and surface tractions $\overline{\mtrx{p}}$
    %
    \begin{align*}
        \delta W_\mathrm{int}( \delta \mtrx{u} )
        & =
        \int_\Omega
        \begin{bmatrix}
            \delta u( x, y ) & \delta v( x, y )  
        \end{bmatrix}
        %
        \begin{bmatrix}
            \overline{X} ( x, y ) \\
            \overline{Y} ( x, y )
        \end{bmatrix}
        \de \Omega
        +
        \int_{\Gamma_u}
        \begin{bmatrix}
            \delta u( x, y ) & \delta v( x, y )  
        \end{bmatrix}
        %
        \begin{bmatrix}
            \overline{p}_x ( x, y ) \\
            \overline{p}_y ( x, y )
        \end{bmatrix}
        \de \Gamma \\
        & = 
        \int_\Omega
         \delta \mtrx{u}( \mtrx{x} )\trn
         \overline{\mtrx{X}}( \mtrx{x} )
        \de \Omega
        +
        \int_{\Gamma_u}
        \delta \mtrx{u}( \mtrx{x} )\trn
        \overline{\mtrx{p}}( \mtrx{x} )
        \de \Gamma
    \end{align*}

    \item Virtual work done by internal forces
    %
    \begin{align*}
        \delta W_\mathrm{int}( \delta \mtrx{u} )
        =
        \int_\Omega
        \begin{bmatrix}
            \delta \varepsilon_x( x, y ) &
            \delta \varepsilon_y[ x, y ) &
            \delta \gamma_{xy}( x, y )             
        \end{bmatrix}
        \begin{bmatrix}
            \sigma_x ( x, y ) \\
            \sigma_y ( x, y ) \\
            \tau_{xy}( x, y )  
        \end{bmatrix}
        \de \Omega
        =
        \int_\Omega
        \delta \mtrx{\varepsilon}( \mtrx{x} )\trn
        \mtrx{\sigma}( \mtrx{x} ) 
    \end{align*}
\end{itemize}

\section{Rectangular finite element}

\begin{itemize}\noitemsep
    \item Nodes 1, 2, 3, and 4 (counter-clockwise)
    \item Defined by node $C$ and lengths $L_x$ and $L_y$
    \item Dimensionless coordinates 
    %
    \begin{align*}
        \xi( x ) & = \frac{2}{L_x}( x - x_C ), &
        \eta( y ) & = \frac{2}{L_y}( y - y_C ),
        \\ 
        \frac{\de \xi}{\de x}
        & = \frac{2}{L_x}, &
        \frac{\de \eta}{\de y}
        & = \frac{2}{L_y}, 
        \\
        \begin{bmatrix}
            \xi \\ \eta
        \end{bmatrix}
        & \in
        [-1, 1]^2, &
        \mtrx{\xi} & \in \widehat{\Omega}
    \end{align*}

    \item One-dimensional basis functions
    %
    \begin{align*}
        \frac{1}{2}( 1 - \xi ), && 
        \frac{1}{2}( 1 + \xi ), &&
        \frac{1}{2}( 1 - \eta ), && 
        \frac{1}{2}( 1 + \eta ),   
    \end{align*}
    %
    yield 4 basis functions in total
    %
    \begin{align*}
        N_1( \xi, \eta ) & = \frac{1}{4}( 1 - \xi )( 1 - \eta ), & 
        N_2( \xi, \eta ) & = \frac{1}{4}( 1 + \xi )( 1 - \eta ), \\
        N_1( \xi, \eta ) & = \frac{1}{4}( 1 - \xi )( 1 + \eta ), & 
        N_2( \xi, \eta ) & = \frac{1}{4}( 1 + \xi )( 1 + \eta )
    \end{align*}

    \item Element strains 
    %
    \begin{align*}
        \begin{bmatrix}
            u( \xi, \eta ) \\
            v( \xi, \eta )
        \end{bmatrix}         
        & \approx
        \begin{bmatrix}
            N_1 & 0 & N_2 & 0 & N_3 & 0 & N_4 & 0 \\
            0 & N_1 & 0 & N_2 & 0 & N_3 & 0 & N_4 
        \end{bmatrix}(\xi, \eta) 
        \begin{bmatrix}
            u_1 \\ v_1 \\
            u_2 \\ v_2 \\
            u_3 \\ v_3 \\
            u_4 \\ v_4 
        \end{bmatrix}
        \\
        \mtrx{u}( \mtrx{\xi} )
        & \approx
        \mtrx{N}\elem{e}( \mtrx{\xi} ) \mtrx{d}\elem{e}
    \end{align*}

    \item Strain components, e.g. 
    %
    \begin{align*}
        \varepsilon_x( \xi(x), \eta(y) )
        =
        \frac{\partial}{\partial x}
        u( \xi(x), \eta(y) )
        =
        \frac{\partial}{\partial \xi} u(\xi, \eta) \frac{\de \xi}{\de x} 
        +
        \frac{\partial}{\partial \eta} u(\xi, \eta) \frac{\de \eta}{\de x} 
        =
        \frac{2}{L_x} \frac{\partial u}{\partial \eta} (\xi, \eta)
    \end{align*}

    \item By analogy
    %
    \begin{align*}
        \begin{bmatrix}
            \varepsilon_x ( \xi, \eta ) \\
            \varepsilon_y ( \xi, \eta ) \\
            \gamma_{xy}( \xi, \eta )  
        \end{bmatrix}
        =
        \frac{2}{L_x L_y}
        \begin{bmatrix}
            L_y \partial / \partial \xi & 0 \\
            0 & L_x \partial / \partial \eta \\
            L_y \partial / \partial \eta & L_y \partial / \partial \xi
        \end{bmatrix}
        %
        \begin{bmatrix}
            u(\xi, \eta) \\
            v(\xi, \eta)
        \end{bmatrix},
        &&
        \mtrx{\varepsilon}( \mtrx{\xi} )
        =
        \mtrx{\partial}_\mtrx{\xi}
        \mtrx{u}( \mtrx{\xi} )
    \end{align*}

    \item Element strains
    %
    \begin{align*}
        \mtrx{\varepsilon}( \mtrx{\xi} )
        \approx 
        \mtrx{\partial}_\mtrx{\xi}
        \mtrx{N}\elem{e}( \mtrx{\xi} ) \mtrx{d}\elem{e}
        =
        \mtrx{B}\elem{e}( \mtrx{\xi} ) \mtrx{d}\elem{e}
    \end{align*}

    \item Element $\mtrx{B}\elem{e}$ matrix~(determined with MATLAB)

\end{itemize}

\begin{align*}%\small
    \frac{1}{2 A\elem{e}}
    \begin{bmatrix} 
        L_{y}(\eta -1) & 0 & -L_{y}(\eta -1) & 0 & -L_{y}(\eta +1) & 0 & L_{y}(\eta +1) & 0\\ 0 & L_{x}(\xi -1) & 0 & -L_{x}(\xi +1) & 0 & -L_{x}(\xi -1) & 0 & L_{x}(\xi +1)\\ L_{x}(\xi -1) & L_{y}(\eta -1) & -L_{x}(\xi +1) & -L_{y}(\eta -1) & -L_{x}(\xi -1) & -L_{y}(\eta +1) & L_{x}(\xi +1) & L_{y}(\eta +1) 
\end{bmatrix}
\end{align*}

\begin{itemize}\noitemsep
    \item Virual fields
    %
    \begin{align*}
        \delta \mtrx{u}( \mtrx{\xi }) 
        \approx 
        \mtrx{N}\elem{e}( \mtrx{\xi} ) \delta \mtrx{d}\elem{e},
        &&
        \delta \mtrx{\varepsilon}( \mtrx{\xi }) 
        \approx 
        \mtrx{B}\elem{e}( \mtrx{\xi} ) \delta \mtrx{d}\elem{e}
    \end{align*}

    \item Virtual work done by internal forces
    %
    \begin{align*}
        \delta W_\mathrm{int}( \delta \mtrx{d}\elem{e} )
        & \approx
        \int_{\Omega\elem{e}}
        ( \mtrx{B}\elem{e}( \mtrx{\xi} ) \delta \mtrx{d}\elem{e} )\trn 
        \mtrx{D}
        \mtrx{B}\elem{e}( \mtrx{\xi} ) \mtrx{d}\elem{e}
        \de \Omega
        =
        ( \delta \mtrx{d}\elem{e} )\trn
        \int_{\Omega\elem{e}}
        \mtrx{B}\elem{e}( \mtrx{\xi} ) \trn
        \mtrx{D}
        \mtrx{B}\elem{e}( \mtrx{\xi} ) 
        \de \Omega \,
        \mtrx{d}\elem{e}
        \\
        & = ( \delta \mtrx{d}\elem{e} )\trn \mtrx{K}\elem{e} \mtrx{d}\elem{e}
    \end{align*}

    \item Substitution
    %
    \begin{align*}
        \de\Omega = \de x \de y = \frac{1}{4} L_x L_y = \frac{1}{4} A\elem{e}
        \Rightarrow
        \mtrx{K}\elem{e} 
        =
        \frac{A\elem{e}}{4} \int_{\widehat{\Omega}}
        \mtrx{B}\elem{e}( \mtrx{\xi} ) \trn
        \mtrx{D}
        \mtrx{B}\elem{e}( \mtrx{\xi} ) 
        \de \widehat{\Omega}
    \end{align*}

    \item Evaluation in MATLAB
    
    \item By similar arguments
    %
    \begin{align*}
        \delta W_\mathrm{int}( \delta \mtrx{d}\elem{e} )
        & \approx
        ( \delta \mtrx{d}\elem{e} )\trn 
        \left( 
        \mtrx{f}_\Omega\elem{e}
        +
        \mtrx{f}_\Gamma\elem{e}
        \right)
        \\
        \mtrx{f}_\Omega\elem{e} 
        & = 
        \int_{\Omega\elem{e}} \mtrx{N}\elem{e}( \mtrx{x} )\trn
        \overline{\mtrx{X}}( \mtrx{x} ) 
        \de \Omega
        \\ 
        \mtrx{f}_\Gamma\elem{e} 
        & = 
        \int_{\Omega\elem{e}} \mtrx{N}\elem{e}( \mtrx{x} )\trn
        \overline{\mtrx{p}}( \mtrx{x} ) 
        \de \Gamma
    \end{align*}


\end{itemize}

\includepdf[pages=1-]{codes/lecture_08.pdf}

\end{document}