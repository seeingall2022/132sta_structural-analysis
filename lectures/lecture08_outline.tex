\documentclass{STA_lectures}

\usepackage{amssymb}
\usepackage{pdfpages}

\newcommand{\mtrx}[1]{{\boldsymbol #1}}
\newcommand{\trn}{^\mathsf{T}}
\newcommand{\elem}[1]{^{(#1)}}
\newcommand{\de}{\, \mathrm{d}}


\newcommand{\recall}{\textcolor{gray}{[recall]}}

\begin{document}
    
\maketitle{8}{2D elasticity: Finite Element Method}

\section{Principle of virtual work}

\begin{itemize}\noitemsep

    \item Virtual work done by external forces~(body forces $\overline{\mtrx{X}}$ and surface tractions $\overline{\mtrx{p}}$
    %
    \begin{align*}
        \delta W_\mathrm{int}( \delta \mtrx{u} )
        & =
        \int_\Omega
        \begin{bmatrix}
            \delta u( x, y ) & \delta v( x, y )  
        \end{bmatrix}
        %
        \begin{bmatrix}
            \overline{X} ( x, y ) \\
            \overline{Y} ( x, y )
        \end{bmatrix}
        \de \Omega
        +
        \int_{\Gamma_u}
        \begin{bmatrix}
            \delta u( x, y ) & \delta v( x, y )  
        \end{bmatrix}
        %
        \begin{bmatrix}
            \overline{p}_x ( x, y ) \\
            \overline{p}_y ( x, y )
        \end{bmatrix}
        \de \Gamma \\
        & = 
        \int_\Omega
         \delta \mtrx{u}( \mtrx{x} )\trn
         \overline{\mtrx{X}}( \mtrx{x} )
        \de \Omega
        +
        \int_{\Gamma_p}
        \delta \mtrx{u}( \mtrx{x} )\trn
        \overline{\mtrx{p}}( \mtrx{x} )
        \de \Gamma
    \end{align*}

    \item Virtual work done by internal forces
    %
    \begin{align*}
        \delta W_\mathrm{int}( \delta \mtrx{u} )
        =
        \int_\Omega
        \begin{bmatrix}
            \delta \varepsilon_x( x, y ) &
            \delta \varepsilon_y[ x, y ) &
            \delta \gamma_{xy}( x, y )             
        \end{bmatrix}
        \begin{bmatrix}
            \sigma_x ( x, y ) \\
            \sigma_y ( x, y ) \\
            \tau_{xy}( x, y )  
        \end{bmatrix}
        \de \Omega
        =
        \int_\Omega
        \delta \mtrx{\varepsilon}( \mtrx{x} )\trn
        \mtrx{\sigma}( \mtrx{x} ) 
    \end{align*}
\end{itemize}

\section{Interpolation, differentiation, and intergration on a rectangular element}

\begin{itemize}\noitemsep
    \item Nodes 1, 2, 3, and 4 (counter-clockwise)
    \item Local coordinate system centered at $C$ 
    \item Auxiliary parameters
    %
    \begin{align*}
        L_x = x_2 - x_1 &&
        L_y = y_3 - y_1 &&
        A = L_x L_y
    \end{align*}

    \item One-dimensional basis functions
    %
    \begin{align*}
        \frac{1}{L_x}( \frac{L_x}{2} - x ), && 
        \frac{1}{L_x}( \frac{L_x}{2} + x ), &&
        \frac{1}{L_x}( \frac{L_y}{2} - y ), && 
        \frac{1}{L_x}( \frac{L_y}{2} + y ),   
    \end{align*}
    
    \item Dimensionless coordinates $\xi(x) = 2 x / L_x$ and $\eta(x) = 2 y / L_y$, 
    transformation to the reference domain $\widehat{\Omega}^{(e)} = (-1, 1)^2$ 
    %
    \begin{align*}
        x( \xi ) = \frac{L_x}{2} \xi, &&
        y( \eta ) = \frac{L_y}{2} \eta, &&
        \leftarrow 
        \mtrx{x}( \mtrx{\xi})
    \end{align*}
    \item Dimensionless basis functions
    %
    \begin{align*}
        \frac{1}{2}( 1 - \xi ), && 
        \frac{1}{2}( 1 + \xi ), &&
        \frac{1}{2}( 1 - \eta ), && 
        \frac{1}{2}( 1 + \eta ),   
    \end{align*}
    %
    yield 4 basis functions in total
    %
    \begin{align*}
        N_1( \xi, \eta ) & = \frac{1}{4}( 1 - \xi )( 1 - \eta ), & 
        N_2( \xi, \eta ) & = \frac{1}{4}( 1 + \xi )( 1 - \eta ), \\
        N_3( \xi, \eta ) & = \frac{1}{4}( 1 + \xi )( 1 + \eta ), & 
        N_4( \xi, \eta ) & = \frac{1}{4}( 1 - \xi )( 1 + \eta )
    \end{align*}

    \item Substitutions
    %
    \begin{align*}
        \frac{\partial}{\partial x} 
        = 
        \frac{\partial \xi}{\partial x} 
        \frac{\partial}{\partial \xi}
        =
        \frac{2}{L_x} \frac{\partial}{\partial \xi},
        &&
        \frac{\partial}{\partial y} 
        = 
        \frac{\partial \eta}{\partial y} 
        \frac{\partial}{\partial \eta}
        =
        \frac{2}{L_y} \frac{\partial}{\partial \eta},
        &&
        \de x \de y = \frac{L_x L_y}{4} \de \xi \de \eta
    \end{align*}

\end{itemize}

\section{Element displacements and strains}

\begin{itemize}

    \item Element displacements
    %
    \begin{align*}
        \begin{bmatrix}
            u( \xi, \eta ) \\
            v( \xi, \eta )
        \end{bmatrix}         
        & \approx
        \begin{bmatrix}
            N_1 & 0 & N_2 & 0 & N_3 & 0 & N_4 & 0 \\
            0 & N_1 & 0 & N_2 & 0 & N_3 & 0 & N_4 
        \end{bmatrix}(\xi, \eta) 
        \begin{bmatrix}
            u_1 \\ v_1 \\
            u_2 \\ v_2 \\
            u_3 \\ v_3 \\
            u_4 \\ v_4 
        \end{bmatrix}
        \\
        \mtrx{u}( \mtrx{\xi} )
        & \approx
        \mtrx{N}\elem{e}( \mtrx{\xi} ) \mtrx{d}\elem{e}
    \end{align*}

    \item Element strains
    %
    \begin{align*}
        \mtrx{\varepsilon}( \mtrx{\xi} )
        =
        \mtrx{\partial}_{\mtrx{x}} 
        \mtrx{u}( \mtrx{\xi} )
        \approx 
        \mtrx{\partial}_{\mtrx{x}}
        \mtrx{N}\elem{e}( \mtrx{\xi} ) \mtrx{d}\elem{e}
        =
        \mtrx{B}\elem{e}( \mtrx{\xi} ) \mtrx{d}\elem{e}
    \end{align*}
    
\end{itemize}

\begin{itemize}
    \item Element $\mtrx{B}\elem{e}$ matrix~(determined with MATLAB)
    %
    \begin{align*}
        \mtrx{B}\elem{e}( \mtrx{\xi} )
        & =
        \begin{bmatrix}
            \partial / \partial x & 0 \\
            0 & \partial / \partial y \\
            \partial / \partial y & \partial / \partial x
        \end{bmatrix}
        \begin{bmatrix}
            N_1 & 0 & N_2 & 0 & N_3 & 0 & N_4 & 0 \\
            0 & N_1 & 0 & N_2 & 0 & N_3 & 0 & N_4 
        \end{bmatrix}(\xi, \eta)
        \\ 
        & = 
        \begin{bmatrix}
            (2 / L_x) \partial / \partial \xi & 0 \\
            0 & (2 / L_y)\partial / \partial \eta \\
            (2 / L_y) \partial / \partial \eta & 
            (2 / L_x) \partial / \partial \xi
        \end{bmatrix}
        \begin{bmatrix}
            N_1 & 0 & N_2 & 0 & N_3 & 0 & N_4 & 0 \\
            0 & N_1 & 0 & N_2 & 0 & N_3 & 0 & N_4 
        \end{bmatrix}(\xi, \eta)
    \end{align*}

    \item Evaluated in MATLAB~(\verb!lecture_08_supplement.mlx!)
    %
    \begin{align*}\hspace*{-6.5em}
        \mtrx{B}\elem{e}( \xi, \eta )
        =
        \frac{1}{2 A}
        \begin{bmatrix} 
            -( 1 - \eta ) L_y & 0 & 
             ( 1 - \eta ) L_y & 0 & 
             ( 1 + \eta ) L_y & 0 &
            -( 1 + \eta ) L_y & 0 \\
            0 & -( 1 - \xi ) L_x & 
            0 & -( 1 + \xi ) L_x & 
            0 &  ( 1 + \xi ) L_x & 
            0 &  ( 1 - \xi ) L_x \\
            -( 1 - \xi ) L_x & -( 1 - \eta ) L_y & 
            -( 1 + \xi ) L_x &  ( 1 - \eta ) L_y &
             ( 1 + \xi ) L_x &  ( 1 + \eta ) L_y & 
             ( 1 - \xi ) L_x & -( 1 + \eta ) L_y  
    \end{bmatrix}
    \end{align*}
    %
    linear both in $(\xi, \eta)$ and therefore in $(x,y)$.

    \item Virtual fields
    %
    \begin{align*}
        \delta \mtrx{u}( \mtrx{x }) 
        \approx 
        \mtrx{N}\elem{e}( \mtrx{x} ) \delta \mtrx{d}\elem{e},
        &&
        \delta \mtrx{\varepsilon}( \mtrx{x }) 
        \approx 
        \mtrx{B}\elem{e}( \mtrx{x} ) \delta \mtrx{d}\elem{e}
    \end{align*}
\end{itemize}

\section{Matrix of equivalent nodal forces}

\begin{itemize}

\item Virtual work done by external forces
%
\begin{align*}
    \delta W^{(e)}_\mathrm{int}( \delta \mtrx{u} )
    & =
    \int_{\Omega^{(e)}}
     \delta \mtrx{u}( \mtrx{x} )\trn
     \overline{\mtrx{X}}( \mtrx{x} )
    \de \Omega
    +
    \int_{\Gamma^{(e)}}
    \delta \mtrx{u}( \mtrx{x} )\trn
    \overline{\mtrx{p}}( \mtrx{x} )
    \de \Gamma
    \\
    & =
    \int_{\Omega^{(e)}}
    ( \mtrx{N}\elem{e}( \mtrx{x} ) \delta \mtrx{d}\elem{e} )\trn
     \overline{\mtrx{X}}( \mtrx{x} )
    \de \Omega
    +
    \int_{\Gamma^{(e)}}
    ( \mtrx{N}\elem{e}( \mtrx{x} ) \delta \mtrx{d}\elem{e} )\trn
    \overline{\mtrx{p}}( \mtrx{x} )
    \de \Gamma
    =
    ( \delta \mtrx{d}\elem{e} )\trn 
    \left( 
    \mtrx{f}_\Omega\elem{e}
    +
    \mtrx{f}_\Gamma\elem{e}
    \right)
\end{align*}
%
with
%
\begin{align*}
    \mtrx{f}_\Omega\elem{e} 
    = 
    \int_{\Omega\elem{e}} \mtrx{N}\elem{e}( \mtrx{x} )\trn
    \overline{\mtrx{X}}( \mtrx{x} ) 
    \de \Omega,
    &&
    \mtrx{f}_\Gamma\elem{e} 
    = 
    \int_{\Gamma\elem{e}} \mtrx{N}\elem{e}( \mtrx{x} )\trn
    \overline{\mtrx{p}}( \mtrx{x} ) 
    \de \Gamma
\end{align*}

\item Example: Body forces $\overline{X}$ constant over the element~[evaluated in MATLAB~(\verb!lecture_08_supplement.mlx!)]
%
\begin{align*}
    \mtrx{f}_\Omega\elem{e} 
    & = 
    \int_{-L_x / 2}^{L_x / 2}
    \int_{-L_y / 2}^{L_y / 2}
    \mtrx{N}\elem{e}( x, y )\trn
    \begin{bmatrix}
        \overline{X} \\ \overline{Y}
    \end{bmatrix}
    \de x \de y
    = 
    \frac{A}{4}
    \int_{-1}^{1} \int_{-1}^{1} \mtrx{N}\elem{e}( \xi, \eta )\trn
    \de \xi \de \eta \,
    \begin{bmatrix}
        \overline{X} \\ \overline{Y}
    \end{bmatrix}
    \\
    & =
    \frac{A}{4}
    \begin{bmatrix}
        \overline{X} & \overline{Y} &
        \overline{X} & \overline{Y} &
        \overline{X} & \overline{Y} &
        \overline{X} & \overline{Y} 
    \end{bmatrix}\trn
\end{align*}

\item Example: Tractions $\overline{p}$ constant over edge $2$--$3$~[evaluated in MATLAB~(\verb!lecture_08_supplement.mlx!)]
%
\begin{align*}
    \mtrx{f}_\Gamma\elem{e} 
    & = 
    \int_{-L_y / 2}^{L_y / 2} \mtrx{N}\elem{e}( L_x/2, y )\trn
    \begin{bmatrix}
        \overline{p}_x \\ \overline{p}_y
    \end{bmatrix} 
    \de y
    =
    \frac{L_y}{2}
    \int_{-1}^{1} \mtrx{N}\elem{e}(1, \eta) \de \eta\trn  
    \begin{bmatrix}
        \overline{p}_x \\ \overline{p}_y
    \end{bmatrix}
    \\ 
    & =
    \frac{L_y}{2}
    \begin{bmatrix}
        0 & 0 & \overline{p}_x & \overline{p}_y &
        \overline{p}_x & \overline{p}_y & 0 & 0
    \end{bmatrix}
\end{align*}

\end{itemize}

\section{Element stiffness matrix}

\begin{itemize}
    \item Virtual work done by internal forces
    %
    \begin{align*}
        \delta W_\mathrm{int}( \delta \mtrx{d}\elem{e} )
        & \approx
        \int_{\Omega\elem{e}}
        ( \mtrx{B}\elem{e}( \mtrx{x} ) \delta \mtrx{d}\elem{e} )\trn 
        \mtrx{D}
        \mtrx{B}\elem{e}( \mtrx{x} ) \mtrx{d}\elem{e}
        \de \Omega
        =
        ( \delta \mtrx{d}\elem{e} )\trn
        \int_{\Omega\elem{e}}
        \mtrx{B}\elem{e}( \mtrx{x} ) \trn
        \mtrx{D}
        \mtrx{B}\elem{e}( \mtrx{x} ) 
        \de \Omega \,
        \mtrx{d}\elem{e}
        \\
        & = ( \delta \mtrx{d}\elem{e} )\trn \mtrx{K}\elem{e} \mtrx{d}\elem{e}
    \end{align*}

    \item Element stiffness matrix
    %
    \begin{align*}
        \mtrx{K}\elem{e} 
        =
        \int_{-L_x / 2}^{L_x / 2} \int_{-L_y / 2}^{L_y / 2}
        \mtrx{B}\elem{e}( x, y ) \trn
        \mtrx{D}
        \mtrx{B}\elem{e}( x, y ) 
        \de x \, \de y
        =
        \frac{A}{4}
        \int_{-1}^{1} \int_{-1}^{1}
        \mtrx{B}\elem{e}( \xi, \eta ) \trn
        \mtrx{D}
        \mtrx{B}\elem{e}( \xi, \eta ) 
        \de \xi \, \de \eta
    \end{align*}

    \item Evaluation in MATLAB~(\verb!lecture_08_supplement.mlx!)
    
\end{itemize}

\section{Element stresses}

\begin{itemize}

\item Expressed in reference coordinates
%
\begin{align*}
    \mtrx{\sigma}^{(e)}( \mtrx{\xi} )
    =
    \mtrx{D} \mtrx{\varepsilon}^{(e)}( \mtrx{\xi} )
    \approx 
    \mtrx{D} \mtrx{B}^{(e)}( \mtrx{\xi} ) \mtrx{d}^{(e)},
\end{align*}
%
linear in $\xi$ and $\eta$ and hence in $x$ and $y$.

\item Stress in element center
%
\begin{align*}
    \mtrx{\sigma}^{(e)}( \mtrx{0} )
    \approx 
    \mtrx{D} \mtrx{B}^{(e)}( \mtrx{0} ) \mtrx{d}^{(e)},
\end{align*}

\end{itemize}

\section{Example}

Consider the a two-dimensional body simultaneously subjected a uniformly distributed load $w = 100$~kNm$^{-1}$ and a concentrated load $P = 12.5$~kN as shown in the figure below. Determine the displacements in all nodes, reactions in the supported nodes, and stresses in the centers of each element. Take the Young's modulus $E = 210$~GPa, Poisson ratio~$\nu = 0.3$, and thickness $t = 0.025$~m.  

\begin{center}
    \includegraphics[width=.8\textwidth]{figures/lecture08_figure01}
\end{center}

\includepdf[pages=1-]{codes/lecture_08_example.pdf}

\includepdf[pages=1-]{codes/lecture_08_supplement.pdf}

\end{document}