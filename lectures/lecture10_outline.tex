\documentclass{STA_lectures}

\usepackage{amssymb}
\usepackage{pdfpages}
\usepackage{import}

\newcommand{\mtrx}[1]{{\boldsymbol #1}}
\newcommand{\trn}{^\mathsf{T}}
\newcommand{\elem}[1]{^{(#1)}}
\newcommand{\de}{\, \mathrm{d}}

\newcommand{\recall}{\textcolor{gray}{[recall]}}

\begin{document}
    
\maketitle{10}{Bending of thin plates: Finite Element Method}

\section{Matrix notation}

\begin{itemize}\noitemsep

    \item Draw the Tonti diagram for $\mtrx{x} \in \Omega$
    
    \item Displacement $w(x, y) = w( \mtrx{x} )$

    \item Curvature
    %
    \begin{align*}
        \begin{bmatrix}
            \kappa_x( x, y ) \\
            \kappa_y( x, y ) \\
            \kappa_{xy}(x, y) 
        \end{bmatrix}
        &&
        \mtrx{\kappa}( \mtrx{x} )
    \end{align*}

    \item Specific bending moments
    %
    \begin{align*}
        \begin{bmatrix}
            m_x( x, y ) \\
            m_y( x, y ) \\
            m_{xy}(x, y) 
        \end{bmatrix}
        &&
        \mtrx{m}( \mtrx{x} )
    \end{align*}

    \item Distributed load $f_z(x, y) = f_z( \mtrx{x} )$ 

    \item Deflection-to-curvature~(geometric) relations
    %
    \begin{align*}
        \begin{bmatrix}
            \kappa_x( x, y ) \\
            \kappa_y( x, y ) \\
            \kappa_{xy}(x, y) 
        \end{bmatrix}
        =
        -
        \begin{bmatrix}
            \partial^2 / \partial x^2 \\
            \partial^2 / \partial y^2 \\
            2 \partial^2 / \partial x \partial y
        \end{bmatrix}
        w(x, y)
        &&
        \mtrx{\kappa}( \mtrx{x} )     
        =
        -
        \mtrx{\partial}^2 w( \mtrx{x} )   
    \end{align*}

    \item Equilibrium equations
    %
    \begin{align*}
        \begin{bmatrix}
            \partial^2 / \partial x^2 &
            \partial^2 / \partial y^2 &
            2 \partial^2 / \partial x \partial y
        \end{bmatrix}
        \begin{bmatrix}
            m_x( x, y ) \\
            m_y( x, y ) \\
            m_{xy}(x, y) 
        \end{bmatrix}
        +
        f_z(x, y)
        = 0
        &&
        \left( \mtrx{\partial}^2 \right)\trn
        \mtrx{m}( \mtrx{x} )
        +
        f_z( \mtrx{x} )
        =
        0
    \end{align*}

    \item Constitutive equations
    %
    \begin{align*}
        \begin{bmatrix}
            m_x( x, y ) \\
            m_y( x, y ) \\
            m_{xy}(x, y) 
        \end{bmatrix}
        =
        \frac{E h^3}{12(1 - \nu^2)}
        \begin{bmatrix}
            1 & \nu & 0 \\
          \nu & 1   & 0 \\
            0 &   0 & \frac{1}{2}( 1 - \nu )
        \end{bmatrix}
        \begin{bmatrix}
            \kappa_x( x, y ) \\
            \kappa_y( x, y ) \\
            \kappa_{xy}(x, y) 
        \end{bmatrix}
        &&
        \mtrx{m}( \mtrx{x} )
        = 
        \mtrx{D}_\mathrm{b} 
        \mtrx{\kappa}( \mtrx{x} ) 
    \end{align*}
\end{itemize}

\section{Principle of virtual work}

\begin{itemize}

    \item Virtual work done by external forces
    %
    \begin{align*}
        \delta W_\mathrm{ext}( \delta w )
        & =
        \int_\Omega
        \delta w( x, y )
        f_z( x, y )
        \de \Omega
        =
        \int_\Omega
        \delta w( \mtrx{x} )
        f_z( \mtrx{x} )
        \de \Omega
    \end{align*}

    \item Virtual work done by internal forces
    %
    \begin{align*}
        \delta W_\mathrm{int}( \delta \mtrx{u} )
        =
        \int_\Omega
        \begin{bmatrix}
            \delta \kappa_x( x, y ) &
            \delta \kappa_y( x, y ) &
            \delta \kappa_{xy}( x, y )             
        \end{bmatrix}
        \begin{bmatrix}
            m_x ( x, y ) \\
            m_y ( x, y ) \\
            m_{xy}( x, y )  
        \end{bmatrix}
        \de \Omega
        =
        \int_\Omega
        \delta \mtrx{\kappa}( \mtrx{x} )\trn
        \mtrx{m}( \mtrx{x} ) 
        \de \Omega
    \end{align*}
\end{itemize}

\section{Rectangular finite element}

\begin{itemize}\noitemsep
    \item Same as for plane elasticity~(reference element $\widehat{\Omega}$, dimensionless coordidates $\xi$ and $\eta$)

    \item For each node $i = 1, \ldots, 4$
    %
    \begin{align*}
        \begin{bmatrix}
            w_i \\
            \varphi_{x, i} \\
            \varphi_{y, i}
        \end{bmatrix}
        =
        \begin{bmatrix}
            w( x_i, y_i ) \\
            - \partial w / \partial y( x_i, y_i ) \\
            \partial w / \partial x( x_i, y_i )
        \end{bmatrix},
        &&
        \mtrx{d}_i
    \end{align*}

    \item $4 \times 3 = 12$ degrees of freedom
    
    \item Melosh~\cite{Melosh:1961:SMA} and Zienkiewicz and Cheung~\cite{Zienkiewicz:1964:FEM} (MZC)~element
    %
    \begin{align*}
        w(\xi, \eta) & \approx 
        C_1 + C_2 \xi + C_3 \eta + C_4 \xi^2 + C_5 \xi \eta 
        + C_6 \eta^2 + C_7 \eta^3 + C_8 \xi^2 \eta 
        + C_9 \xi \eta^2 \\
        & + C_{10} \eta^3 + C_{11} \xi^3 \eta + C_{12} \xi \eta^3       
    \end{align*}

    \item Following the same procedure as for beam elements, we can obtain
    %
    \begin{align*}
        w( \xi, \eta )
        & =
        \sum_{i=1}^4 
        \left( 
            N^w_i(\xi, \eta) w_i +  
            N^\varphi_{x, i}(\xi, \eta) \varphi_{x, i} + 
            N^\varphi_{y, i}(\xi, \eta) \varphi_{y, i}
        \right) \\
        & =
        \sum_{i=1}^4 
        \begin{bmatrix}
            N_i(\xi, \eta) & 
            N^\varphi_{x, i}(\xi, \eta) & 
            N^\varphi_{y, i}(\xi, \eta) 
        \end{bmatrix}
        \begin{bmatrix}
            w_i \\
            \varphi_{x, i} \\
            \varphi_{y, i}
        \end{bmatrix}
        =
        \sum_{i=1}^4 
        \mtrx{N}_i( \xi, \eta ) \mtrx{d}_i
    \end{align*}
    %
    with $\mtrx{\xi}^\mathrm{n} = [ -1;  1;  1; -1]$ and $\mtrx{\eta}^\mathrm{n} = [ -1;  1;  1; -1]$:
    %
    \begin{align*}
        N^w_i(\xi, \eta) & = \frac{1}{8}
        \left( 1 + \xi^\mathrm{n}_i \xi \right)
        \left( 1 + \eta^\mathrm{n}_i \eta \right)
        \left( 
            2 + \xi^\mathrm{n}_i \xi + \eta^\mathrm{n}_i \eta
            - \xi^2 - \eta^2  
        \right)
        \\
        N^\varphi_{x, i}(\xi, \eta) & = 
        \frac{L_y}{16}
        \left( 1 - \eta^2 \right)
        \left( \eta + \eta^\mathrm{n}_i \right)
        \left( 1 + \xi^\mathrm{n}_i \xi \right) 
        \\
        N^\varphi_{y, i}(\xi, \eta) & = 
        \frac{L_x}{16} 
        \left( \xi^2 - 1 \right)
        \left( \xi + \xi^\mathrm{n}_i \right)
        \left( 1 + \eta^\mathrm{n}_i \eta \right) 
    \end{align*}
    %
    \begin{align*}
        w( \mtrx{\xi} )
        & \approx
        \begin{bmatrix}
            \mtrx{N}_1( \mtrx{\xi} ) &
            \mtrx{N}_2( \mtrx{\xi} ) & 
            \mtrx{N}_3( \mtrx{\xi} ) &
            \mtrx{N}_4( \mtrx{\xi} )   
        \end{bmatrix}
        \begin{bmatrix}
            \mtrx{d}_1 \\
            \mtrx{d}_2 \\
            \mtrx{d}_3 \\
            \mtrx{d}_4
        \end{bmatrix}
        =
        \mtrx{N}\elem{e}( \mtrx{\xi} ) \mtrx{d}\elem{e} \\
        \delta w( \mtrx{\xi} )
        & \approx
        \mtrx{N}\elem{e}( \mtrx{\xi} ) \delta\mtrx{d}\elem{e}
    \end{align*}

\end{itemize}

\subsection{Work done by external forces~$\delta W_\mathrm{ext}$}

\begin{itemize}

    \item Recall that    
    %
    \begin{align*}
        \delta W_\mathrm{ext}( \delta w )
        & =
        \int_{-L_x / 2}^{L_x / 2}
        \int_{-L_y / 2}^{L_y / 2}
        \delta w( x, y )
        f_z( x, y )
        \de x \de y
        =
        \frac{L_x L_y}{4}
        \int_{-1}^{1}
        \int_{-1}^{1}
        \delta w( \xi, \eta )
        f_z( \xi, \eta )
        \de \xi \de \eta
        \\
        & =
        \frac{A}{4}
        \int_{\widehat{\Omega}}
        \left( \mtrx{N}\elem{e}( \mtrx{\xi} ) \delta\mtrx{d}\elem{e} \right)\trn
        f_z( \mtrx{\xi} )
        \de \mtrx{\xi}
        =
        ( \delta\mtrx{d}\elem{e} ) \trn
        \frac{A}{4}
        \int_{\widehat{\Omega}}
        \mtrx{N}\elem{e}( \mtrx{\xi} )\trn
        f_z( \mtrx{\xi} )
        \de \mtrx{\xi}
        =
        ( \delta\mtrx{d}\elem{e} ) \trn
        \mtrx{f}_\Omega\elem{e}
    \end{align*}

    \item Example: $f_z = q$~(const)
    %
    computed using MATLAB~(\verb!lecture_10_supplement.mlx!)

    \item Gauss quadrature $\mtrx{\xi}^\mathrm{g} = [-1, 1, 1, -1] / \sqrt{3}$ and $\mtrx{\eta}^\mathrm{g} = [-1, -1, 1,  1] / \sqrt{3}$
    %
    \begin{align*}
        \int_{\widehat{\Omega}} f(\xi, \eta) \de \xi \de \eta
        \approx
        \sum_{i=1}^{2} \sum_{j=1}^{2}
        f( \eta^\mathrm{g}_i, \eta^\mathrm{g}_j )
    \end{align*}

    \item \emph{Exact} for functions cubic in $\xi$ and $\eta$

\end{itemize}

\subsection{Work done by internal forces~$\delta W_\mathrm{int}$}

\begin{itemize}

    \item Evaluated using numerical integration

    \item Curvature~($\mtrx{x} \rightarrow \mtrx{\xi}$)
    %
    \begin{align*}
        \mtrx{\kappa}( \mtrx{x} )
        = -
        \begin{bmatrix}
            \partial^2 / \partial x^2 \\
            \partial^2 / \partial y^2 \\
            2 \partial^2 / \partial x \partial y
        \end{bmatrix}
        w( \mtrx{x} )
        && \rightarrow &&
        \mtrx{\kappa}( \mtrx{\xi} )
        = -        
        \begin{bmatrix}
            ( 2 / L_x )^2 \, \partial^2 / \partial \xi^2 \\
            ( 2 / L_y )^2 \, \partial^2 / \partial \eta^2 \\
            2 ( 2 / L_x ) ( 2 / L_y ) \, \partial^2 / \partial \xi \partial \eta
        \end{bmatrix}
        w( \mtrx{\xi} )
    \end{align*}

    \item $\mtrx{B}$ matrix for thin plate bending
    %
    \begin{align*}
        \mtrx{\kappa}( \xi, \eta )
        \approx
        \begin{bmatrix}
            \mtrx{B}_1( \xi, \eta ) &
            \mtrx{B}_2( \xi, \eta ) & 
            \mtrx{B}_3( \xi, \eta ) &
            \mtrx{B}_4( \xi, \eta )   
        \end{bmatrix}
        \begin{bmatrix}
            \mtrx{d}_1 \\
            \mtrx{d}_2 \\
            \mtrx{d}_3 \\
            \mtrx{d}_4
        \end{bmatrix}
        =
        \mtrx{B}\elem{e}( \xi, \eta ) \mtrx{d}\elem{e}
    \end{align*}
    %
    with
    %
    \begin{align*}
        \mtrx{B}_i ( \xi, \eta ) = -
        \begin{bmatrix}
            4 / L^2_x \, \partial^2 / \partial \xi^2 \\
            4 / L_y^2 \, \partial^2 / \partial \eta^2 \\
            8/  ( L_x L_y ) \partial^2 / \partial \xi \partial \eta
        \end{bmatrix}
       \begin{bmatrix}
            N^w_i( \xi, \eta ) & 
            N^\varphi_{x, i}( \xi, \eta ) & 
            N^\varphi_{y, i}( \xi, \eta ) 
        \end{bmatrix}
    \end{align*}

    \item Evaluated using MATLAB~(\verb!lecture_10_supplement.mlx!)
   
    \item Virtual curvatures
    %
    \begin{align*}
        \delta \mtrx{\kappa}( \xi, \eta ) 
        \approx 
        \mtrx{B}\elem{e}( \xi, \eta ) \delta \mtrx{d}\elem{e}
    \end{align*}

    \item Virtual work done by internal forces
    %
    \begin{align*}
        \delta W_\mathrm{int}
        & \approx
        \int_{-L_x / 2}^{L_x / 2}
        \int_{-L_y / 2}^{L_y / 2}
        ( \mtrx{B}\elem{e}( x, y ) \delta \mtrx{d}\elem{e} )\trn 
        \mtrx{D}_\mathrm{b}
        \mtrx{B}\elem{e}( x, y ) \mtrx{d}\elem{e}
        \de x \de y
        \\
        & =
        ( \delta \mtrx{d}\elem{e} )\trn
        \int_{-L_x / 2}^{L_x / 2}
        \int_{-L_y / 2}^{L_y / 2}
        \mtrx{B}\elem{e}( x, y ) \trn
        \mtrx{D}_\mathrm{b}
        \mtrx{B}\elem{e}( x, y ) 
        \de x \de y \,
        \mtrx{d}\elem{e}
        \\
        & =
        ( \delta \mtrx{d}\elem{e} )\trn
        \frac{L_x L_y}{4}
        \int_{-1}^{1}
        \int_{-1}^{1}
        \mtrx{B}\elem{e}( \xi, \eta ) \trn
        \mtrx{D}_\mathrm{b}
        \mtrx{B}\elem{e}( \xi, \eta ) 
        \de \xi \de \eta \,
        \mtrx{d}\elem{e}
        \\
        & = 
        ( \delta \mtrx{d}\elem{e} )\trn \mtrx{K}_\mathrm{b}\elem{e} \mtrx{d}\elem{e}
    \end{align*}

    \item Gauss quadrature
    %
    \begin{align*}
        \mtrx{K}_\mathrm{b}\elem{e} 
        =
        \frac{L_x L_y}{4}
        \sum_{j=1}^4
        \mtrx{B}\elem{e}( \xi^\mathrm{g}_j, \eta^\mathrm{g}_j ) \trn
        \mtrx{D}_\mathrm{b}
        \mtrx{B}\elem{e}( \xi^\mathrm{g}_j, \eta^\mathrm{g}_j )
    \end{align*}

    \item Available as \verb!stiffness_matrix_plate.m!
    
\end{itemize}

\section{Internal forces}

\begin{itemize}

    \item Employ the~(generalized) constitutive equations
    %
    \begin{align*}
        \mtrx{m}( \xi, \eta )
        =
        \mtrx{D}_\mathrm{b}
        \mtrx{\kappa}( \xi, \eta )
        \approx
        \mtrx{D}_\mathrm{b}
        \mtrx{B}\elem{e}( \xi, \eta ) 
        \mtrx{d}\elem{e}
    \end{align*}

    \item Values at the element center
    %
    \begin{align*}
        \mtrx{m}( 0, 0 )
        \approx
        \mtrx{D}_\mathrm{b}
        \mtrx{B}\elem{e}( 0, 0 ) 
        \mtrx{d}\elem{e}
    \end{align*}
   
    \item Available as \verb!internal_forces_plate.m!

\end{itemize}

\clearpage

\section{Example~(\texttt{lecture\_10.mlx})}

\begin{center}
    \def\svgwidth{0.475\textwidth}
    \import{figures/}{lecture10_figure01.pdf_tex}
\end{center}

\References

\includepdf[pages=1-]{codes/lecture_10_supplement.pdf}

\end{document}