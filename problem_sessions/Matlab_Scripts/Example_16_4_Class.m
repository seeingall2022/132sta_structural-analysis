clc; % clears text from Command Window
clear all; % clears the workspace 
close all; % close all figure windows

%% Input parameter
E = 200 * 1e9; % Youngs modulus N/m^2
A = 5.16 * 1e3 * 1e-6; % cross section area in m^2
I = 200 * 1e6 * 1e-12; % geometrical moment of inertia in m^4

L1 = 4; % length of element 1 in m
L2 = 2; % length of element 2 in m
L3 = 4; % length of element 3 in m
Coo = [0 0; 0 4; 2 4; 2 0]

%% Element stiffness matrices

% Transformation matrix
% Member 1
lamx = (Coo(2,1)-Coo(1,1))/L1;
lamy = (Coo(2,2)-Coo(1,2))/L1;
k1 = stiffmat(E,A,I,L1,lamx,lamy)

% Member 2
lamx = (Coo(3,1)-Coo(2,1))/L2;
lamy = (Coo(3,2)-Coo(2,2))/L2;
k2 = stiffmat(E,A,I,L2,lamx,lamy)

% Member 3
lamx = (Coo(4,1)-Coo(3,1))/L3;
lamy = (Coo(4,2)-Coo(3,2))/L3;
k3 = stiffmat(E,A,I,L3,lamx,lamy)

%% Global stiffness matrix

K = zeros(12,12);
% K(7:9,7:9) = k1(1:3,1:3); % red

k = zeros(6,6,3); % defining a 6x6x3 array for the local stiffness matrices
k(:,:,1) = k1; % filling the first layer with k1
k(:,:,2) = k2; % filling the second layer with k2
k(:,:,3) = k3; % filling the third layer with k3

% table of global degrees of freedom for each element
% first row element 1, and so on
TDOF = [7 8 9 1 2 3;
        1 2 3 4 5 6;
        4 5 6 10 11 12];

for iElem = 1:3
    for iRow = 1:6
        for iCol = 1:6
            K(TDOF(iElem,iRow),TDOF(iElem,iCol)) = K(TDOF(iElem,iRow),TDOF(iElem,iCol)) + k(iRow,iCol,iElem);
        end
    end
end

%% Solving the system of equations

F = zeros(12,1); % force vector filled with zeros
F(1) = 18000; % horizontal force in N
F(6) = 68000; % moment in Nm

D = zeros(12,1); % displacement vector filled with zeros

D(1:6) = inv(K(1:6,1:6))*F(1:6)

% Connectivity matrix conatines the global joint number of each element 
Cn = [1 2; 2 3; 3 4];
% rearanging displacement values following the joint numbers
U = zeros(12,1);
U(1:3) = D(7:9); % displacement in joint 1
U(4:6) = D(1:3); % displacement in joint 2
U(7:9) = D(4:6); % displacement in joint 3
U(10:12) = D(10:12); %displacement in joint 4

% Calling the function plotframe 
plotframe(Coo,Cn,U)

% Calculating unkown forces
F(7:12) = K(7:12,1:6) * D(1:6)

%% Function to calculate element stiffness matrix in global coordinates
function k = stiffmat(E,A,I,L,lamx,lamy)
T1 = zeros(6,6);
T1(1,1) = lamx;
T1(1,2) = lamy;
T1(2,1) = -lamy;
T1(2,2) = lamx;
T1(3,3) = 1;
T1(4,4) = lamx;
T1(4,5) = lamy;
T1(5,4) = -lamy;
T1(5,5) = lamx;
T1(6,6) = 1;

k_prime = zeros(6,6);
k_prime(1,1) = A*E/L;
k_prime(1,4) = -A*E/L;
k_prime(4,1) = -A*E/L;
k_prime(4,4) = A*E/L;

k_prime(2,2) = 12*E*I/L^3;
k_prime(2,5) = -12*E*I/L^3;
k_prime(5,2) = -12*E*I/L^3;
k_prime(5,5) = 12*E*I/L^3;

k_prime(2,3) = 6*E*I/L^2;
k_prime(2,6) = 6*E*I/L^2;
k_prime(5,3) = -6*E*I/L^2;
k_prime(5,6) = -6*E*I/L^2;

k_prime(3,2) = 6*E*I/L^2;
k_prime(6,2) = 6*E*I/L^2;
k_prime(3,5) = -6*E*I/L^2;
k_prime(6,5) = -6*E*I/L^2;

k_prime(3,3) = 4*E*I/L;
k_prime(6,6) = 4*E*I/L;
k_prime(6,3) = 2*E*I/L;
k_prime(3,6) = 2*E*I/L;


k = T1'*k_prime*T1;
end


%% Function to plot deformed frame structure
function gca = plotframe(Points,Cn,U)

% INPUT 
%Points = [0 0; 5 0; 8 -6];        %Nodes
%Cn = [1 2; 2 3];                  %connectivity matrix
ne = length(Cn);                  %no. of elements
%U = [0 0 0 0 0 -0.8 1.5 0 0.7]';  %deformation in global system

% PLOTTING
clf
hold on

% Simple plot for intial & deformed shapes
Uplot = U; Uplot(3:3:end) = [];
NewPoints = Points + reshape(Uplot,size(Points'))';
for i = 1:ne
    plot(Points(Cn(i,:),1), ...
        Points(Cn(i,:),2),'bo--')     %initial
    plot(NewPoints(Cn(i,:),1),...
        NewPoints(Cn(i,:),2),'go')  %deformed
end

for i = 1:ne
    
    % Length of element 
    X = Points(Cn(i,2),1)-Points(Cn(i,1),1);
    Y = Points(Cn(i,2),2)-Points(Cn(i,1),2);
    L = sqrt(X^2+Y^2);
    
    % Angle & transformation matrix
    B = atand(Y/X);
    c = cosd(B);
    s = sind(B);
    T = [c s 0 0 0 0; -s c 0 0 0 0;
        0 0 1 0 0 0; 0 0 0 c s 0;
        0 0 0 -s c 0; 0 0 0 0 0 1];
    
    % Index number in the global system
    index = [3*Cn(i,1)-2:3*Cn(i,1)...
        3*Cn(i,2)-2:3*Cn(i,2)];
    
    u = T*U(index);  %local dofs
    x = 0:L/20:L;    %original local axis
    
    % local horizontal displacements function, u(x)
    N1bar = 1-x/L;
    N2bar = x/L;
    xx = x + N1bar*u(1) + N2bar*u(4);
    
    % local vertical displacements function, w(x)
    N1beam = -3*x.^2/L^2 + 1 + 2*x.^3/L^3;
    N2beam = -2*x.^2/L + x + x.^3/L^2;
    N3beam = 3*x.^2/L^2 - 2*x.^3/L^3;
    N4beam = -x.^2/L + x.^3/L^2;
    yy = N1beam*u(2) + N2beam*u(3) + N3beam*u(5) + N4beam*u(6);

    % adjustment to the global axis (move the origin + rotate)
    xr = Points(Cn(i,1),1) + xx*cosd(B) - yy*sind(B);
    yr = Points(Cn(i,1),2) + xx*sind(B) + yy*cosd(B);

    %plot(xx,yy,'c-') %plot at local axis (to check)
    plot(xr,yr,'k-','LineWidth',1)  %plot at global axis  
    
end
hold off
axis equal
end