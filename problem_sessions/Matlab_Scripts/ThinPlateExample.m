clc;
clear all;
close all;

%% Input parameter

% Numerical values
E_num = 210 *1e9; % Youngs modulus in N/m^2
nu_num = 0.3; % Poisson ratio
t_num = 0.2; % thickness in m
a_num = 2; % half length of elements in X1-direction in m
b_num = 2; % half length of elements in X2-direction in m
q = -1000;

k1 = elemstiffmat_thinplate(a_num,b_num,E_num,nu_num,t_num);
f1 = equnodalforce_thinplate(a_num,b_num,q);

%% Assembling the global stiffness matrix

k = zeros(12,12,4);
k(:,:,1) = k1;
k(:,:,2) = k1;
k(:,:,3) = k1;
k(:,:,4) = k1;

TDOF = [1 2 3 4 5 6 13 14 15 10 11 12;...
        4 5 6 7 8 9 16 17 18 13 14 15;...
        10 11 12 13 14 15 22 23 24 19 20 21;...
        13 14 15 16 17 18 25 26 27 22 23 24];
    
K_alt = zeros(27,27);
for iElem=1:4
    for iRow=1:12
        for iCol=1:12
            K_alt(TDOF(iElem,iRow),TDOF(iElem,iCol)) = K_alt(TDOF(iElem,iRow),TDOF(iElem,iCol)) + k(iRow,iCol,iElem);
        end
    end
end

K_alt

f = zeros(12,4);
f(:,1) = f1;
f(:,2) = f1;
f(:,3) = f1;
f(:,4) = f1;

F_alt = zeros(27,1);
for iElem=1:4
    for iRow=1:12
        F_alt(TDOF(iElem,iRow)) = F_alt(TDOF(iElem,iRow)) + f(iRow,iElem);
    end
end

K_red = K_alt([13 14 15],[13 14 15]);
F_red = F_alt([13 14 15]);

D = zeros(27,1);
D([13 14 15]) = inv(K_red)*F_red

%% Function to assemble the elemnt stiffness matrix for a thin plate
function [f] = equnodalforce_thinplate(a_num,b_num,q)
syms a b real;
    f = [1/4 a/12 b/12 ...
         1/4 -a/12 b/12 ...
         1/4 -a/12 -b/12 ...
         1/4 a/12 -b/12];
    f = f *4*q*a*b;
    f = subs(f,a,a_num); % substituting the symbol a with the value a_num
    f = subs(f,b,b_num);
    f = transpose(f);
    f = double(f);
end
%% Function to assemble the elemnt stiffness matrix for a thin plate
function [k] = elemstiffmat_thinplate(a_num,b_num,E_num,nu_num,t_num)
% Symbols
syms a b X1 X2 E nu t real;

%% Element Stiffnes matrix

%k_a = zeros(12,12);
k_a(2,1) = 6*a;

k_a(1,1) = 6;
k_a(1,2) = 6*a;
k_a(1,4) = -6;
k_a(1,5) = 6*a;
k_a(1,7) = -3;
k_a(1,8) = 3*a;
k_a(1,10) = 3;
k_a(1,11) = 3*a;

k_a(2,1) = 6*a;
k_a(2,2) = 8*a^2;
k_a(2,4) = -6*a;
k_a(2,5) = 4*a^2;
k_a(2,7) = -3*a;
k_a(2,8) = 2*a^2;
k_a(2,10) = 3*a;
k_a(2,11) = 4*a^2;

k_a(4,1) = -6;
k_a(4,2) = -6*a;
k_a(4,4) = 6;
k_a(4,5) = -6*a;
k_a(4,7) = 3;
k_a(4,8) = -3*a;
k_a(4,10) = -3;
k_a(4,11) = -3*a;

k_a(5,1) = 6*a;
k_a(5,2) = 4*a^2;
k_a(5,4) = -6*a;
k_a(5,5) = 8*a^2;
k_a(5,7) = -3*a;
k_a(5,8) = 4*a^2;
k_a(5,10) = 3*a;
k_a(5,11) = 2*a^2;

k_a(7,1) = -3;
k_a(7,2) = -3*a;
k_a(7,4) = 3;
k_a(7,5) = -3*a;
k_a(7,7) = 6;
k_a(7,8) = -6*a;
k_a(7,10) = -6;
k_a(7,11) = -6*a;

k_a(8,1) = 3*a;
k_a(8,2) = 2*a^2;
k_a(8,4) = -3*a;
k_a(8,5) = 4*a^2;
k_a(8,7) = -6*a;
k_a(8,8) = 8*a^2;
k_a(8,10) = 6*a;
k_a(8,11) = 4*a^2;

k_a(10,1) = 3;
k_a(10,2) = 3*a;
k_a(10,4) = -3;
k_a(10,5) = 3*a;
k_a(10,7) = -6;
k_a(10,8) = 6*a;
k_a(10,10) = 6;
k_a(10,11) = 6*a;

k_a(11,1) = 3*a;
k_a(11,2) = 4*a^2;
k_a(11,4) = -3*a;
k_a(11,5) = 2*a^2;
k_a(11,7) = -6*a;
k_a(11,8) = 4*a^2;
k_a(11,10) = 6*a;
k_a(11,11) = 8*a^2;

k_a(12,:) = zeros(1,11);
k_a(:,12) = zeros(12,1);

k_a = b/(6*a^3);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% k_b
k_b(3,1) = 6*b;

k_b(1,1) = 6;
k_b(1,3) = 6*b;
k_b(1,4) = 3;
k_b(1,6) = 3*b;
k_b(1,7) = -3;
k_b(1,9) = 3*b;
k_b(1,10) = -6;
k_b(1,12) = 6*b;

k_b(3,1) = 6*b;
k_b(3,3) = 8*b^2;
k_b(3,5) = 3*b;
k_b(3,6) = 4*b^2;
k_b(3,7) = -3*b;
k_b(3,9) = 2*b^2;
k_b(3,10) = -6*b;
k_b(3,12) = 4*b^2;

k_b(4,1) = 3;
k_b(4,3) = 3*b;
k_b(4,4) = 6;
k_b(4,6) = 6*b;
k_b(4,7) = -6;
k_b(4,9) = 6*b;
k_b(4,10) = -3;
k_b(4,12) = 3*b;

k_b(6,1) = 3*b;
k_b(6,3) = 4*b^2;
k_b(6,4) = 6*b;
k_b(6,6) = 8*b^2;
k_b(6,7) = -6*b;
k_b(6,9) = 4*b^2;
k_b(6,10) = -3*b;
k_b(6,12) = 2*b^2;

k_b(7,1) = -3;
k_b(7,3) = -3*b;
k_b(7,4) = -6;
k_b(7,6) = -6*b;
k_b(7,7) = 6;
k_b(7,9) = -6*b;
k_b(7,10) = 3;
k_b(7,12) = -3*b;

k_b(9,1) = 3*b;
k_b(9,3) = 2*b^2;
k_b(9,4) = 6*b;
k_b(9,6) = 4*b^2;
k_b(9,7) = -6*b;
k_b(9,9) = 8*b^2;
k_b(9,10) = -3*b;
k_b(9,12) = 4*b^2;

k_b(10,1) = -6;
k_b(10,3) = -6*b;
k_b(10,4) = -3;
k_b(10,6) = -3*b;
k_b(10,7) = 3;
k_b(10,9) = -3*b;
k_b(10,10) = 6;
k_b(10,12) = -6*b;

k_b(12,1) = 6*b;
k_b(12,3) = 4*b^2;
k_b(12,4) = 3*b;
k_b(12,6) = 2*b^2;
k_b(12,7) = -3*b;
k_b(12,9) = 4*b^2;
k_b(12,10) = -6*b;
k_b(12,12) = 8*b^2;

k_b = a/(6*b^3);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% k_c
k_c(2,1) = a;

k_c(1,1) = 1;
k_c(1,2) = a;
k_c(1,3) = b;
k_c(1,4) = -1;
k_c(1,6) = -b;
k_c(1,7) = 1;
k_c(1,10) = -1;
k_c(1,11) = -a;

k_c(2,1) = a;
k_c(2,3) = 2*a*b;
k_c(2,10) = -a;

k_c(3,1) = b;
k_c(3,2) = 2*a*b;
k_c(3,4) = -b;

k_c(4,1) = -1;
k_c(4,3) = -b;
k_c(4,4) = 1;
k_c(4,5) = -a;
k_c(4,6) = b;
k_c(4,7) = -1;
k_c(4,8) = a;
k_c(4,10) = 1;

k_c(5,4) = -a;
k_c(5,6) = -2*a*b;
k_c(5,7) = a;

k_c(6,1) = -b;
k_c(6,4) = b;
k_c(6,5) = -2*a*b;

k_c(7,1) = 1;
k_c(7,4) = -1;
k_c(7,5) = a;
k_c(7,7) = 1;
k_c(7,8) = -a;
k_c(7,9) = -b;
k_c(7,10) = -1;

k_c(8,4) = a;
k_c(8,7) = -a;
k_c(8,9) = 2*a*b;

k_c(9,7) = -b;
k_c(9,8) = 2*a*b;
k_c(9,10) = b;

k_c(10,1) = -1;
k_c(10,2) = -a;
k_c(10,4) = 1;
k_c(10,7) = -1;
k_c(10,9) = b;
k_c(10,10) = 1;
k_c(10,11) = a;
k_c(10,12) = -b;

k_c(11,1) = -a;
k_c(11,10) = a;
k_c(11,12) = -2*a*b;

k_c(12,10) = -b;
k_c(12,11) = -2*a*b;

k_c = nu/(2*a*b)*k_c;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% k_d
k_d(2,1) = 3*a;

k_d(1,1) = 21;
k_d(1,2) = 3*a;
k_d(1,3) = 3*b;
k_d(1,4) = -21;
k_d(1,5) = 3*a;
k_d(1,6) = -3*b;
k_d(1,7) = 21;
k_d(1,8) = -3*a;
k_d(1,9) = -3*b;
k_d(1,10) = -21;
k_d(1,11) = -3*a;
k_d(1,12) = 3*b;

k_d(2,1) = 3*a;
k_d(2,2) = 8*a^2;
k_d(2,4) = -3*a;
k_d(2,5) = -2*a^2;
k_d(2,7) = 3*a;
k_d(2,8) = 2*a^2;
k_d(2,10) = -3*a;
k_d(2,11) = -2*a^2;

k_d(3,1) = 3*b;
k_d(3,3) = 8*b^2;
k_d(3,4) = -3*b;
k_d(3,6) = -8*b^2;
k_d(3,7) = 3*b;
k_d(3,9) = 2*b^2;
k_d(3,10) = -3*b;
k_d(3,12) = -8*b^2;

k_d(4,1) = -21;
k_d(4,2) = -3*a;
k_d(4,3) = -3*b;
k_d(4,4) = 21;
k_d(4,5) = -3*a;
k_d(4,6) = 3*b;
k_d(4,7) = -21;
k_d(4,8) = 3*a;
k_d(4,9) = 3*b;
k_d(4,10) = 21;
k_d(4,11) = 3*a;
k_d(4,12) = -3*b;

k_d(5,1) = 3*a;
k_d(5,2) = -2*a^2;
k_d(5,4) = -3*a;
k_d(5,5) = 8*a^2;
k_d(5,7) = 3*a;
k_d(5,8) = -8*a^2;
k_d(5,10) = -3*a;
k_d(5,11) = 2*a^2;

k_d(6,1) = -3*b;
k_d(6,3) = -8*b^2;
k_d(6,4) = 3*b;
k_d(6,6) = 8*b^2;
k_d(6,7) = -3*b;
k_d(6,9) = -2*b^2;
k_d(6,10) = 3*b;
k_d(6,12) = 2*b^2;

k_d(7,1) = 21;
k_d(7,2) = 3*a;
k_d(7,3) = 3*b;
k_d(7,4) = -21;
k_d(7,5) = 3*a;
k_d(7,6) = -3*b;
k_d(7,7) = 21;
k_d(7,8) = -3*a;
k_d(7,9) = -3*b;
k_d(7,10) = -21;
k_d(7,11) = -3*a;
k_d(7,12) = 3*b;

k_d(8,1) = -3*a;
k_d(8,2) = 2*a^2;
k_d(8,4) = 3*a;
k_d(8,5) = -8*a^2;
k_d(8,7) = -3*a;
k_d(8,8) = 8*a^2;
k_d(8,10) = 3*a;
k_d(8,11) = -2*a^2;

k_d(9,1) = -3*b;
k_d(9,3) = 2*b^2;
k_d(9,4) = 3*b;
k_d(9,6) = -2*b^2;
k_d(9,7) = -3*b;
k_d(9,9) = 8*b^2;
k_d(9,10) = 3*b;
k_d(9,12) = -8*b^2;

k_d(10,1) = -21;
k_d(10,2) = -3*a;
k_d(10,3) = -3*b;
k_d(10,4) = 21;
k_d(10,5) = -3*a;
k_d(10,6) = 3*b;
k_d(10,7) = -21;
k_d(10,8) = 3*a;
k_d(10,9) = 3*b;
k_d(10,10) = 21;
k_d(10,11) = 3*a;
k_d(10,12) = -3*b;

k_d(11,1) = -3*a;
k_d(11,2) = -2*a^2;
k_d(11,4) = 3*a;
k_d(11,5) = 2*a^2;
k_d(11,7) = -3*a;
k_d(11,8) = -2*a^2;
k_d(11,10) = 3*a;
k_d(11,11) = 8*a^2;

k_d(12,1) = 3*b;
k_d(12,3) = -8*b^2;
k_d(12,4) = -3*b;
k_d(12,6) = 2*b^2;
k_d(12,7) = 3*b;
k_d(12,9) = -8*b^2;
k_d(12,10) = -3*b;
k_d(12,12) = 8*b^2;

k_d = k_d*(1-nu)/(30*a*b);

k = (E*t^3)/(12*(1-nu^2))*(k_a + k_b + k_c + k_d);

k = subs(k,a,a_num); % substituting the symbol a with the value a_num
k = subs(k,b,b_num);
k = subs(k,E,E_num);
k = subs(k,nu,nu_num);
k = subs(k,t,t_num);
k = double(k)
end