clc;
clear all;
close all;

%% Variables and Coordinates
A = 1;
E = 200;
L1 = 2;
L2 = 2*sqrt(2);
Coo = [0 0; 2 0; 2 2; 0 2];

%% Element stiffness and transformation matrices
% lamx = (x2 - x1)/L
% lamy = (y2 - y1)/L
% lamx lamy 0 0
% 0 0 lamx lamy

% Member 1
lamx = (Coo(2,1)-Coo(1,1))/L1;
lamy = (Coo(2,2)-Coo(1,2))/L1;
T1 = [lamx lamy 0 0; 0 0 lamx lamy];
%pinv(T1)

% Member 2
lamx = (Coo(3,1)-Coo(1,1))/L2;
lamy = (Coo(3,2)-Coo(1,2))/L2;
T2 = [lamx lamy 0 0; 0 0 lamx lamy];
%pinv(T2)

% Member 3
lamx = (Coo(4,1)-Coo(1,1))/L1;
lamy = (Coo(4,2)-Coo(1,2))/L1;
T3 = [lamx lamy 0 0; 0 0 lamx lamy];

% Member 4
lamx = (Coo(3,1)-Coo(4,1))/L1;
lamy = (Coo(3,2)-Coo(4,2))/L1;
T4 = [lamx lamy 0 0; 0 0 lamx lamy];

% Member 5
lamx = (Coo(2,1)-Coo(4,1))/L2;
lamy = (Coo(2,2)-Coo(4,2))/L2;
T5 = [lamx lamy 0 0; 0 0 lamx lamy];

% Member 6
lamx = (Coo(3,1)-Coo(2,1))/L1;
lamy = (Coo(3,2)-Coo(2,2))/L1;
T6 = [lamx lamy 0 0; 0 0 lamx lamy];

%% Element stiffnes matrix

k_prime = [1 -1; -1 1];

k1 = T1'*A*E/L1*k_prime*T1;
k2 = T2'*A*E/L2*k_prime*T2;
k3 = T3'*A*E/L1*k_prime*T3;
k4 = T4'*A*E/L1*k_prime*T4;
k5 = T5'*A*E/L2*k_prime*T5;
k6 = T6'*A*E/L1*k_prime*T6;

%% Global stiffnes matrix

% creating an 8 by 8 matrix filled with zeros
K = zeros(8,8);

% filling the global stiffnes matrix - lines 1 and 2
K(1:2,1:2) = k1(1:2,1:2) + k2(1:2,1:2) + k3(1:2,1:2); %red
K(1:2,3:4) = k3(1:2,3:4); % green 
K(1:2,5) = k1(1:2,4); % blue
K(1:2,6) = k1(1:2,3); % blue
K(1:2,7:8) = k2(1:2,3:4); % brown

% filling the global stiffnes matrix - lines 3 and 4
K(3:4,1:2) = k3(3:4,1:2);
K(3:4,3:4) = k3(3:4,3:4) + k4(1:2,1:2) + k5(1:2,1:2);
K(3:4,5) = k5(1:2,4);
K(3:4,6) = k5(1:2,3);
K(3:4,7:8) = k4(1:2,3:4); 

% filling the global stiffnes matrix - lines 5 and 6
K(5,1:2) = k1(4,1:2); % red
K(6,1:2) = k1(3,1:2); % red
K(5,3:4) = k5(4,1:2); % green
K(6,3:4) = k5(3,1:2); % green
K(5,5) = k1(4,4) + k5(4,4) + k6(4,4); % blue
K(5,6) = k1(4,3) + k5(4,3) + k6(4,3); % blue
K(6,5) = k1(3,4) + k5(3,4) + k6(3,4); % blue
K(6,6) = k1(3,3) + k5(3,3) + k6(3,3); % blue
K(5,7:8) = k6(2,3:4);
K(6,7:8) = k6(1,3:4);

% filling the global stiffnes matrix - lines 7 and 8
K(7:8,1:2) = k2(3:4,1:2);
K(7:8,3:4) = k4(3:4,1:2);
K(7:8,5) = k6(3:4,2);
K(7:8,6) = k6(3:4,1);
K(7:8,7:8) = k2(3:4,3:4) + k4(3:4,3:4) + k6(3:4,3:4);
%% Global stiffnes matrix alternative
%{
k = zeros(4,4,6);
k(:,:,1) = k1;
k(:,:,2) = k2;
k(:,:,3) = k3;
k(:,:,4) = k4;
k(:,:,5) = k5;
k(:,:,6) = k6;

TDOF = [1 2 6 5;...
        1 2 7 8;...
        1 2 3 4;...
        3 4 7 8;...
        3 4 6 5;...
        6 5 7 8];
    
K_alt = zeros(8,8);
for iElem=1:6
    for iRow=1:4
        for iCol=1:4
            K_alt(TDOF(iElem,iRow),TDOF(iElem,iCol)) = K_alt(TDOF(iElem,iRow),TDOF(iElem,iCol)) + k(iCol,iRow,iElem);
        end
    end
end
%}
%% Solving system of equations 
% Force voector with two external forces
F = zeros(5,1);
F(3) = 2;
F(4) = -4;
% displacement vector
D = zeros(8,1);
% solving the equations
D(1:5) = inv(K(1:5,1:5))*F;

% Plotting the deformed and undeformed truss structure
figure(2);hold on
plot([Coo(1,1) Coo(2,1)],[Coo(1,2) Coo(2,2)],'-ok')
plot([Coo(2,1) Coo(3,1)],[Coo(2,2) Coo(3,2)],'-ok')
plot([Coo(3,1) Coo(4,1)],[Coo(3,2) Coo(4,2)],'-ok')
plot([Coo(4,1) Coo(1,1)],[Coo(4,2) Coo(1,2)],'-ok')
plot([Coo(1,1) Coo(3,1)],[Coo(1,2) Coo(3,2)],'-ok')
plot([Coo(4,1) Coo(2,1)],[Coo(4,2) Coo(2,2)],'-ok')

plot([Coo(1,1)+D(1) Coo(2,1)+D(6)],[Coo(1,2)+D(2) Coo(2,2)+D(5)],'-or')
plot([Coo(2,1)+D(6) Coo(3,1)+D(7)],[Coo(2,2)+D(5) Coo(3,2)+D(8)],'-or')
plot([Coo(3,1)+D(7) Coo(4,1)+D(3)],[Coo(3,2)+D(8) Coo(4,2)+D(4)],'-or')
plot([Coo(4,1)+D(3) Coo(1,1)+D(1)],[Coo(4,2)+D(4) Coo(1,2)+D(2)],'-or')
plot([Coo(1,1)+D(1) Coo(3,1)+D(7)],[Coo(1,2)+D(2) Coo(3,2)+D(8)],'-or')
plot([Coo(4,1)+D(3) Coo(2,1)+D(6)],[Coo(4,2)+D(4) Coo(2,2)+D(5)],'-or')


% Calculating the unkown forces/support reactions
F(6:8) = K(6:8,1:5) * D(1:5);

% Calculating the member forces
f1 = A*E/L1*k_prime*T1*[D(1:2);D(3:4)];
f2 = A*E/L2*k_prime*T2*[D(1:2);D(7:8)];
f3 = A*E/L1*k_prime*T3*[D(1:2);D(3:4)];
f4 = A*E/L1*k_prime*T4*[D(3:4);D(7:8)];
f5 = A*E/L2*k_prime*T5*[D(3:4);D(6);D(5)];
f6 = A*E/L1*k_prime*T6*[D(7:8);D(6);D(5)];