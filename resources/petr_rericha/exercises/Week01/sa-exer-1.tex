\documentclass[12pt]{article}
\usepackage{graphics,../my,../comb}
\usepackage{makeidx}
\graphicspath{{figures/}}
%\newlength{\figwidth}
%\newcommand{\insinFig}[3]
%   {\makebox[0pt][l]{\raisebox{-#1\figwidth}{\hspace{#2\figwidth} #3}}}
%\newcommand{\insFig}[2]
%   {\makebox[#2][l]{\resizebox{#2}{!}
%   {\rotatebox{-90}{\includegraphics{#1}}}}}

\oddsidemargin 0mm

\renewcommand{\sketch}{Sketch}
\renewcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{STA course, exercise and homework 1}
\author{Petr \v Re\v richa}
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt
The default units used in numerical examples are $m$, $kN$, $kNm$, $K$ (the same
as $C^0$ in the context of the course since we deal with temperature differences
exclusively). We thus do not indicate units in examples as a rule.
This is valid for the whole course. In design practice the units must be given
at least for the results!
\\ \\
Some conventions we use (for those who have not passed the subject $SM3E$)\\ 
$[~]$ brackets denote matrix (in the context of this course mostly a square matrix)\\
$\{~\}$ figure brackets denote a column matrix, for brevity very often called
  a vector.\\
$t$ thermal load, homogeneous temperature change in a beam with respect to
  the reference stress and deformation free state.\\
$\Delta t$ thermal load, the diference of the temperature changes of the upper
   face (strictly speaking the face with the greater $y_l$ coordinate) 
   minus the lower face of the beam. The term temperature gradient is used
   for it throughout the course for brevity.\\
$E$  Young modulus of elasticity\\
$A$ cross-section area\\
$J$ cross-section moment of inertia\\
$h$ cross-section depth\\
conjugate quantities - a pair of quantities whose product is work,
for instance force:displacement, moment:rotation, bending moment:curvature etc.
\\ \\
The exercise complements sections 1 - 2.1  of the lecture notes
'Structural analysis - displacement method for planar frames
and gridworks' on the course web page, just 'Notes'
forthwith. They
should substitute classes of the present 1st week of the semester.
Questions concerning the stuff, address, please, to\\
'petr.rericha@fsv.cvut.cz'.\\ 
The purpose of the first week exercise is 1) establish a common base
for notation, graphics and terminology 2) refresh and unite the
basics on the slope deflection method 3) the stiffness matrix and
lod vector of a straight beam in the local coordinate system.
HW 1 should be delivered at the same address
by tuesday, September, 29th. If possible, process the homework
in a document processor (Word, Latex,..), scanned manuscripts are
acceptable, too. In the latter case, use reasonable resolution so that
the file is not larger than 2MB. pdf, jpg and png formats are accepted.



\subsection{Reference for formulas and variables}
We use a right handed cartesian coordinate system $x, y, z$ throughout the course.
In plane problems, the $x, y$ plane is used. Components of vectors are
are specified by subscripts $_x, _y, _z$, their positive sense is
implicitly in the positive sense of respective coordinate axis.
We use only the local coordinate system in this section, see section 2 of
the Notes. Its positive $x$ axis goes from end joint $i$ to end joint $j$
of the beam where $i$ and $j$ are the numbers of frame  joints where
the beam is attached. Note that the same beam has two possible local 
coordinate systems depending on the order of the joint numbers.
The order is either specified explicitly, for instance beam $15,24$ or
in the superscript of the beam stiffness matrix $[K]^{15,34}$ and/or of the 
transformation matrix $[T]^{15,34}$. The order of subscripts of some other 
variables is irrelevant, for instance beam length and bending stiffness
for prismatic beams $l_{i,j}, k_{i,j}$, the subscripts at the end forces,
$M_{i,j}, V_{i,j}, N_{i,j}$ indicate at which joint the force is acting
(the first subscript) in which beam (the pair of them) but do \emph{not}
define the local coordinate system.



Some variables, for instance internal forces of beams, loads, reactions 
may be defined explicitly by arrows in the sketches then
the arrows define their positive senses and override the implicit definition. 
See for instance the sketches in sections 1 and 2 of the Notes.
When the values of variables are given in numerical solutions
then the arrows in the sketches do NOT define positive sense but simply the
magnitude of the respective variable. The arrow indicates then the actual
action of the variable.
The absolute values (no signs) should then be given in the sketches,
see the pin force load 10 in the frame without sidesway below.

The positive sense of internal forces $N, V, M$ (normal force, shear force and
bending moment) and end forces $X, Y, M$  differs in planar frames:
\\
\insFigUp{int-end-forces}{0.6\textwidth}
\\
$M$ arrows indicate the positive sense of the \emph{end moments} in the sketch.
Positive sense of the bending moment cannot be defined in general.
In individual solutions an 'ad hoc' positive sense of the bending moment
has to be defined when necessary. The correct action of the bending moment
is indicated in the  bending moment diagrams by the orientation of the
moment line - we use the european convention drawing the bending moment
diagram on the \emph{tension} side of the beam base line, see the
diagrams in the sample solution below

The displacements and rotations in real structures are very small, 
both $\varphi_i$ and $\psi_{i,j}$
are of the order $0.001$ in real structures. The geometric relations valid
for infinitesimal displacements and rotations are therefore used,
for instance $\psi_{i,j}=v/l_{i,j}$. 
\\
\insFigUp{end-forces.pdf}{\textwidth}
\begin{equation}
M_{i,j} = k_{i,j} (2\phi_i + \phi_j -3~\psi_{i,j}) + M_{i,j}^f 
                                                        \label{eq_sd4}
\end{equation}
$M_{i,j}$ - end moment at joint $i$ of beam $i,j$ owing to the joint
and beam rotations and to the beam load. The beam load is not shown in the left sketch
in order not to obscure the picture, instead its tokens are visible in the right sketch.
\\
$M_{i,j}^f$ - fixed end moment at joint $i$ of beam $i,j$ owing to the beam load alone
 with joint displacements and rotations fixed.
\\
$\varphi_i$ - joint rotation
\\
$\psi_{i,j}$ - beam rotation
\\
A table of the fixed-end moments is provided for easy reference in the Notes.
A modified formula is valid for the end moment $M_{i,j}$ when end $j$ is attached
to the joint by a hinge:
\\
\insFigUp{end-forces-hinge.pdf}{0.5\textwidth}
\begin{equation}
M_{i,j}= k_{i,j}{3\over 2}(\phi_i-\psi_{i,j}) 
  +M_{i,j}^f-{M_{j,i}^f\over 2}
  ~~~~~~{\rm hinge~at~}j~~~~~~~M_{j,i}=0
                                                        \label{eq_sd6} 
\end{equation}
If a beam is not loaded then a simple formula follows from its free body equilibrum
condition:
\begin{equation}
V_{i,j}= {M_{i,j}+M_{j,i}\over l_{i,j}}                  
\end{equation}

A simple example of a solution by the slope deflection method is provided
to demonstrate (recall) the rules and the basic approach of the method, 
establish the notation and graphic symbols and the way the solution should
be presented in the homeworks and exam tasks.
\subsection{Slope deflection method, frame without sidesway}
\insFigUp{frame1.pdf}{\textwidth}
\\
Degrees of freedom (DOFs) in the slope deflection method and
the displacement method have different
meaning than the same term in the analysis of statical determinacy of
frames (generally bodies). Here they specify and count the ways 
the joints and beams can move. In deciding on that the axial rigidity 
of the beams must be considered in the slope deflection method. 
Each joint can translate in two
 directions and rotate that is, has 3 DOF,
 provided its displacement are not constrained.
Supports and axial rigidity of beams may constrain the DOFs.
Supports and axial rigidity of beams $1,3$ and
$2,3$ imply that joint $3$ cannot translate. Then the rigidity
of beam $3,4$ entails together with the support of joint $4$ that
the joint cannot translate, either. We conclude that the frame has
2DOFs, $\varphi_3$ and $\varphi_4$. The rotation $\varphi_4$ can
be eliminated when formula~\refpar{eq_sd6}.s used instead of~\refpar{eq_sd4}.
Then a single DOF remains. This decision is always the first step of the solution
by the slope deflection method. 
The beams are axially elastic in the displacement method so that in this
structure six (five when $\varphi_4$ is eliminated) DOFs would exist 
in the displacement method!
The structure DOFs should always
be clearly specified in the protocol:\\
DOFs:$\varphi_3=\varphi$.\\ The joint subscript can be omitted in the example
since this the only rotation considered.
\\
Beam stiffnesses are specified relative to the reference one, $k_{ref}=2EJ/l_{1,3}$,
for brevity.
\[ k_{1,3}=k_{2,3}=1,~~~k_{3,4}=0.8 \]
Equilibrium equation in terms of the end forces, conjugate to $\varphi_3$:
\[ 3\odot : M_{3,1}+M_{3,2}+M_{3,4}=0 \]
Everything up to this point does not depend on the load. Two actual load cases
are considered in this example, the LC1 is a force load shown in the figure above,
the LC2 is a thermal load by temperature gradient.
\\
\subsection{Force load}
Expressions for the end moments:
\[M_{3,1}=2\varphi,~~~~M_{3,2}=2\varphi,~~~~M_{3,4}=1.5\cdot 0.8\varphi+
 {4\over5}\cdot10\cdot+\left({2.5^3\over 5^2}\right)(1-(-0.5))=1.2\varphi+7.5 \]
Substitution in the equilibrium equation reads
\[5.2\varphi+7.5=0,~~~\Rightarrow~~~\varphi=-1.44~(\cdot {1\over k_{ref}}) \]
Back substitutions deliver the end moments:
\[ M_{3,1}= 2\cdot(-1.44)=-2.88,~~`M_{3,2}=-2.88;~~~M_{3,4}=1.2\cdot(-1.44)+7.5=5.76 \]
The moments do not depend on the reference stiffness when the load is exclusively
force load (no thermal, kinematic,... load). This trick simplifies the numerics
of the solution.

The two remaining end moments are evaluated:
\[ M_{1,3}= \varphi=-1.44,~~~M_{2,3}=\varphi=-1.44 \]
The bending moment at the center of beam $3,4$ can be computed either by
equilibrium conditions of the free body of the beam or by superposition.
Both ways are illustrated in the two sketches in the above figure.
The first way requires two steps.
\[ beam i,j \odot j: M_{3,4}+ 10\cdot 2 - V_{3.4}\cdot 5=0,~~~
 \Rightarrow~~V_{3,4}=5.16 \]
When ad hoc sign convention is adopted for the bending moment, positive when
lower fibers in tension, then 
\[ M_c= -M_{3,4}+V_{3,4}\cdot 2.5=-5.76+5.16\cdot 2.5=7.1 \]
The other way superimposes the effect on a simple beam of the end moments and
the beam load. At the central cross-section it is
\[ M_c= - 5.76/2+{1\over 4} \cdot 10 {4\over 5} 5=-2.88+10=7.12 \]
The shear forces in beams $1,3$ and $2,3$ are readily available by formula (3),
shear force $V_{3,4}=5.16$ has been computed above.
Normal forces follow from
the free body equilibrium condition of beam $3,4$ in the direction
of the beam axis
\[ -N_{3,4}- 10{3\over 5}=0,~~~\Rightarrow~~N_{3,4}=-6\]
and from the equilibrium conditions of joint 3,
\[ 3\uparrow: -V_{3,2}+{4\over 5}V_{3,4}-{3\over 5}N_{3,4}+N_{3,1}=0,~~~
 \Rightarrow~~N_{3,1}=-8.8\]
\[ 3\rightarrow: V_{3,1}-V_{3,4}{3\over5}+N_{3,2}-N_{3,4}{4\over5}=0,~~~
 \Rightarrow~~N_{3,2}=-0.63 \]

\subsection{Thermal load}
Temperature gradient $\Delta t$ affects beam $3,4$, positive when the upper face
is warmer than the bottom one. This is the sign convention used in the table
of the fixed end moments in Notes.
\\
\insFigUp{frame1-tgrad.pdf}{0.8\textwidth}
\\
All forces in the frame will be expressed in terms of the temperature induced
curvature $\kappa_t= -\Delta t \alpha/h$. The minus sign appears here since for
the curvature and bending moments in beam $3,4$ we adopt the usual ad hoc
sign convention - positive extend the lower fibers.
Unlike it was in the force load solution, the reference beam stiffness 
$k_{ref}= 2EJ/l_{2,3}=EJ/2$
is used here in the expressions (the above force load solution was carried out
with unit reference beam stiffness for the sake of brevity).
From the table of the fixed end moments $M_{3,4}^f=-\kappa_t EJ=. -2\kappa_t k_{ref}$.
\[ M_{3,4}=1.5\cdot0.8k_{ref}\varphi + M_{3,4}^f-0.5(-M_{3,4}^f)= 1.2k_{ref}\varphi
+ 1.5M_{3,4}^f= k_{ref}(1.2\varphi-3\kappa_t)\]
The expressions for the other end moments are the same as in the force load
solution just instead of unit reference beam stiffness the true one is used here.
The equilibrum equation in terms of the joint rotation then reads
\[ 5.2  k_{ref} \varphi-3 k_{ref}\kappa_t=0,~~~\Rightarrow~~\varphi=0.58\kappa_t\]
Note that this time the joint rotation is the true one and need not be
divided by $ k_{ref}$ as it was in the force load solution.
The end moments share the common multiplier $k_{ref}\kappa_t$ and their factors
are in the final moment diagram.
\[ M_{3,4}=k_{ref}(0.7-3)\kappa_t=-2.3k_{ref}\kappa_t,~~~M_{3,1}=2k_{ref}\\varphi=
 1.16k_{ref}\kappa_t=M_{3,2} \]
An observation is made on the occasion: when one end of a beam is fixed, the other
does not translate (just rotates) and the beam is not loaded then end moment
at the fixed end equals one half of the moment at the other end.
\subsection{Examples}
\subsubsection{Determine DOFS for the slope deflection and displacement method}
\insFigUp{exer1-task1.pdf}{0.5\textwidth}
\\
Slope deflection method:\\
DOFs:joint rotations $\varphi_3, \varphi_4$, beam rotation $\psi_{1,3}$
together 3 DOFs.\\
There are two other beam rotations $\psi_{1,4}$ and  $\psi_{3,4}$ but they
depend on $\psi_{1,3}$. Owing to the axial rigidity of beams $1,3$ and
$2,4$, joints $3$ and $4$ can translate only in horizontal direction
(recall the assumption of infinitesimal displacements). Then owing to
rigidity of beam $3,4$ the translations of joints $3$ and $4$ must be the
same, $u$ and beam $3,4$ translates thus horizontally with zero beam
rotation. The beam rotations $\psi_{1,3}$, $\psi_{2,4}$ must meet the
condition $\psi_{1,3}2=\psi_{3,4}3$.
\\
Displacement method:\\
DOFs: all three joint displacement at joints $3$ and $4$, together 6 DOFs.

\subsubsection{Set up the load vector for beam $5,7$}
\insFigUp{exer1-task2.pdf}{0.5\textwidth}
\\
Some explanation is necessary of the sketch tokens. They will be used
throughout the course.
The graphic token for the uniform load indicates load per unit length of the
beam in the direction indicated by the shading, acting towards the beam,
which means vertical down in this case. Thermal load $\Delta t$ is the
the load by the temperature difference between the upper and lower
faces of the beam, called temperature gradient forthwith for brevity.

The uniform load has a component in the direction of the beam axis.
The total force in that direction is $f\cdot5\cdot(3/5)=3$. One half of it
is assigned to each joint, $X_{5.7}=X_{7,5}= 1.5$. 
Next, the fixed end moments are computed with
the aid of the table in section 1 of the Notes:
\[ M_{5,7}= {1\over 12}5^2\cdot{4 \over 5} f +F{l^3\over 8l^2}-
   {\Delta t \alpha E J\over h}= 1.67+3.125-{\Delta t \alpha E J\over h}
   =4.80+{\Delta t \alpha E J\over h} \]
\[ M_{7,5}= -1.67-3.125 +{\Delta t \alpha E J\over h} = 
 -4.80-{\Delta t \alpha E J\over h} \]
 The transverse end forces $Y$ can now be determined by equilibrium
 conditions, for instance
 \[ \uparrow 5: Y_{7,5}\cdot 5 + M_{7,5}+M_{5,7} -f\cdot 5\cdot 2 -F\cdot 5=0,
  ~~~ \Rightarrow Y_{7,5}=4.5 \]
  or by superposition:
  \\
  \insFigUp{exer1-task2-superposition.pdf}{0.5\textwidth}
  \\
The load vector of this beam in the local coordinate system:
\[\{F\}=\{1.5,4.5,4.8-{\Delta t \alpha E J\over h},1.5,4.5,-4.8+
 {\Delta t \alpha E J\over h} \}^T \]
Each component of the column matrix $\{F\}$ (note the transposition mark $^T$)  
has its physical meaning. They are the reactions of the beam whose both
ends are clamped and the beam is loaded by the given load. The order of the
components must agree with the order of the DOFs of the beam, see
formula (4) in the Notes.
\\ \\ \\ \\ \\ \\
{\bf HW 1:}\\
1)Set up the stiffness matrix $[K]$ and the load vector $\{F\}$ of the beam $1,2$
in the local coordinate system. $EJ=32$, $EA=32$, $\Delta t=40^oK$, 
 $\alpha=10^{-3}$, $h=0.5$,$t=10^oK$. Note that the right end is attached by
 a hinge to joint $2$ of the frame.
\\ 2) Explain the physical meaning of elements $K_{3,5}$, $K_{3,3}$ nd
     $F_5$.
\\
\insFigUp{hw1.pdf}{0.5\textwidth}
%\end{document}
\newpage
\noindent
{\bf HW1 solution}\\
ad 1):
Indices $1,2$ of the beam can be omitted since just a single beam is 
considered.
\[ k={2EJ\over l}= 2\cdot 32/8=8,~~~a={EA\over l}= 32/8=4\]
\[ K= \left[ \begin{array}{cccccc} 4&0&0&-4&0&0\\ &0.187&1.5&0&-0.187&0\\
       &&12&0&-1.5&0\\&&&4&0&0\\&&&&0.187&0\\&&&&&0
      \end{array}\right] \]
\[\{F\}^T= \{0.04,4.76,6.16,-0.04,3.24,0\}\]
ad 2):
Element $K_{3,5}$ is the moment at joint 1 when the beam is not
loaded and all DOFs of the
beam are fixed but for a unit vertical displacement at joint 2.
Element $K_{2,3}$ is the vertical force at joint 1 when  the beam is not
loaded and all DOFs of the
beam are fixed but for a unit rotation at joint 1.
$F_5$ is the vertical force at joint 2 when all DOFs are fixed and the
beam is loaded by the given load.

\end{document}
