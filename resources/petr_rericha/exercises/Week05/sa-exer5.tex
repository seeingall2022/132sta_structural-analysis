\documentclass[12pt]{article}
\usepackage{graphics,../my,../comb}
\usepackage{makeidx}
\graphicspath{{figures/}}
%\newlength{\figwidth}
%\newcommand{\insinFig}[3]
%   {\makebox[0pt][l]{\raisebox{-#1\figwidth}{\hspace{#2\figwidth} #3}}}
%\newcommand{\insFig}[2]
%   {\makebox[#2][l]{\resizebox{#2}{!}
%   {\rotatebox{-90}{\includegraphics{#1}}}}}

\oddsidemargin 0mm

\renewcommand{\sketch}{Sketch}
\renewcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{STA course, exercise and homework 5}
\author{Petr \v Re\v richa }
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt
The default units used in numerical examples are $m$, $kN$, $kNm$, $K$ (the same
as $C^0$ in the context of the course since we deal with temperature differences
exclusively). We thus do not indicate units in examples as a rule.
This is valid for the whole course. In design practice the units must be given
at least for the results!
\\ \\
\emph{
The principle of virtual displacements (PVd), useful in construction of the
influence lines, is a particular form of the broader
principle of virtual work. Both are based on the concept of work of
conjugate quantities - a pair of quantities whose product is work,
for instance force:displacement, moment:rotation, bending moment:curvature etc.
}
\\ \\
The exercise complements sections 1 - 4  of the lecture notes
'Structural analysis - influence lines in planar statically
determinate frames' on the course web page, just 'Notes'
forthwith. They together
should substitute classes of the present 5th week of the semester.
Questions concerning the stuff, address, please, to\\
'petr.rericha@fsv.cvut.cz'.\\ 
The purpose of the fifth week's exercise is 1) help understand the concept of
the influence lines and their application, 2) get a skill in drawing
the influence lines.

It is recommended to try to solve each of the examples step by step
on your own first
and only when at loss at an point of the solution, resort to the text.
This is how I tend to teach in contact classes. In this contactless
form it requires more selfcontrol on your part, of course.
\\
HW 5 should be delivered at the same address by Tuesday, October 27th, 2pm.

\subsection{Construction of the influence lines}
Combination of both methods explained in the Notes is preferable in practice.
The PVd alone is very effective and elegant
but sometimes the construction of the virtual displacements can be difficult.
The construction 'by definition' is a simpler concept but requires the solution
of the structure for many 'unit' load cases (the load cases where a unit force
at various positions is the only load). This might be cumbersome. The combination
used in the examples here and recommended to students is specified below.
Each influence line is valid for a single quantity and each quantity has its
influence line. Quantity in this context might be a reaction component, internal
force, deflection and other, all at a single cross-section or point. 
\emph{The influence lines of bending moments need an ad hoc sign convention, 
recall that
bending moment do not have a general sign convention. It is usual in horizontal
girders to assign positive sense to moments which pull at lower fibers. This
is the convention of this course.}
\\
Two steps are recommended for the construction of an influence line:
\\ 1) Try to construct the virtual displacement for the given quantity. If it is too
difficult, try to derive just the shape of the virtual displacment without values.
If you fail to do that either, decide just how many straight pieces the influence
line will have and what kind of continuity/discontinuity will be at the connection
points. 
\\ 2) Choose suitable points (mostly the connection points of the straight lines)
where to position the unit force and solve the asociated load cases. Each load case
supplies an ordinate of the influence line as the value of the quantity in the load
case. These ordinates are used to calibrate the shape of the influence line if it
was determined in the first step (a single ordinate is enough in this case) or
determine the ends of individual straight lines.

\subsection{Example 1, Continuous beam}
\insFigUp{sa-exer5-ex1.pdf}{\textwidth}
\\
Continuous beam is suitable for the first step. The centers of rotation of rigid
parts are always the supports when virtual displacement is imposed. In the example
the shape of each influence line can be drawn right away, but for three exceptions
the ordinates can be determined, too. For those exceptions, 
$\eta M_B$, $\eta M_e$ and $\eta V_e$,
two load cases are solved in the right margin. Some ordinates of those exceptional
influence lines can be taken from these load cases and many others can be checked.

Several observations can be generalized from the example which are worth remembering. 
\emph{Influence line of a bending
moment always is broken but continuous at the respective cross-section} which,
BTW, is the consequence of the PVd (bending moment does work on relative rotation
of the two adjacent parts). \emph{Influence line of a shear force always exhibits jump
of a unit magnitude at the respective cross-section} 
(shear force does work upon relative lateral
translation of the two adjacent parts). Moreover, the adjacent straight lines
must be parallel to each other, because the moment must NOT do work on their
relative rotation (see $\eta V_{BA},~~ \eta V_e$) .
Some other auxiliary rules can be derived but it is beyond the scope of the course.
\\
\subsection{Example 2, frame 3 hinges}
\insFigUp{sa-exer5-ex2.pdf}{\textwidth}
\\
Influence lines shapes are not so easy to guess in this case since the centers of
rotations of the parts are not as obvious as in case of the continous beam.
The straight parts and their connections are thus determined by the PVd,
some ordinates by solving suitable load cases with unit pin force load.
The two load cases necessary are in the right margin of the sketch.
Influence line $\eta A_y$ will consist of two straight lines broken but continuous
at $C$. Two ordinates can be read in the two LC diagrams, 1.5 at the left tip and
0.5 at the center. The left straight part can now be drawn but just a single point
is known of the right straight part. There are two very simple load cases which
need not be drawn, when unit force acts at joints D or E. There are then no reactions
and internal forces except reactions $A_y$ or $B_y$ respectively and the normal forces
in the legs. For other quantities these two load cases exhibit zero values.
When the unit force stands at E, the reaction $A_y=0$ and the other point of the
other straight line is known. In the end it turns out that the influence line is
straight in the whole range. The same deductions can be used in case of $\eta A_x$.
$\eta M_d$ will consist of three straight parts with breaks at $D$ and $C$. 
Two ordinates are read from the two load cases  drawn, two zero ordinates are
at $D$ and $E$, that is enough to draw $\eta M_d$.
$\eta V_d$ again includes three straight lines with jump at cross-section $d$
and potential break at $C$. When the unit force stands above the joint $D$, shear
force must be $0$, when it moves past the cross-section $d$ it must jump by $1$ up
by the PVd and by euilibrium, too.

\subsection{Example 3, three links support}
\insFigUp{sa-exer5-ex3.pdf}{\textwidth}
\\
This frame is suitable for demonstration of the full application of the PVd.
The stuff is not compulsory and does not appear in the exam tests. 
\\
The $\eta A$ influence line is a single straight line by the PVd. However, it is
rather difficult to compute the two necessary ordinates. The virtual displacement
apparently is the rotation of the girder with respect to support E, so that the
point above support E does not move in vertical direction. This point thus is
the zero point of the influence line. In order to get another point following the
recommended steps, a load case is solved as shown in the right margin of the sketch.
The sequence of equilibrium conditions used to solve for reactions and the forces
in the links is rather lengthy and not presented here.

The same can be achieved by the PVd alone. Link A is imposed virtual extension 1.
In the inlay vector diagram in the right margin, drawn with dashed lines, the
displacement vector of  hinge F is found, denoted 'F displ.'. The extension of
the link translates the hinge by the vector denoted '1'. From this point the
hinge can move in the tangent direction of the circle with center at support D,
the set of possible positions is shown by the dashed line perpendicular to the
link axis. At the same time, the distance E-F must remain the same so that
hinge F can move from the start position along line which is perpendicular to
the E-F abscissa. The intersection with the dashed line perpendicular to the
link axis determines the vector 'F displ.', its vertical component gives the
ordinate of the influence line $\eta A$ at hinge F. The graphic construction
is quite simple although the verbal description of it sounds terribly. The
graphic solution is instructive but it would have to be accompanied by numeric
counter part in a real solution in practice.

The construction of the virtual displacement for $\eta M_g$ is in the second
sketch in the right margin. When the relative rotation of parts I and II,
conjugate to   $\eta M_g$, is released (the fictitious hinge in the sketch),
the two parts can move. Part II rotation center is E. The rotation center of part I
, point O, is the intersection of line E-g with line D-F. The lengths E-g and O-g
are the same therefore the rotation angles of the two parts are the same with
opposite signs. There are two zero points of the influence line, one is indicated
in the sketch, the other is above support E. This determines the shape of the influence
line. One ordinate can be taken from the solved load case, 0.865, which the ordinate
at the left tip of the structure. The line is fully specified. 

\subsection{Application of the influence lines}
The purpose of the influence lines is to assist in finding extreme values of variables.
The extremes in their turn are used to assess supports, cross-sections etc. Once
the influence line of a variable is known, the value of the variable can be computed
for any load by integration of the product of the influence line function 
times the load function, see equation (1) in the Notes and the example next to it.

The extreme values are evaluated of some quantities from Example 1. The most
frequent live load in practice is an arbitrarily distributed uniform load of
given intensity. A unit intensity load is assumed for convenience. This means
that the integral in equation (1) in the Notes simplifies to the integral of
the influence line alone. Moreover, the influence lines in statically determinate
frames consist of straight lines (Rule 3.1 in the Notes). The integration
becomes playful.
\\
Reaction $A$:\\
Maximum is achieved when the load is located from A to B, $max(A)=1\cdot 4/2=2$.\\
Minimum is achieved when the load is located from B to D, $min(A)=-0.5\cdot 6/2=-1.5$.
\\
Reaction $B$: $max(B)=1.5\cdot 10/2=7.5$, $min(B)=0$\\
Shear force $V_{BA}$: $max(V_{BA})=0$, $min(V_{BA})=(-1\cdot 4-0.5\cdot 6)/2=-3.5$.\\
\\ \\
HW4:\\
Draw the influence lines of $A_x$, $A_y$, $E_x$, $E_y$, $C$, $M_p$, $V_p$ and $N_p$.
Evaluate the maxima and minima of the quantities for arbitrarily allocated
uniform load intensity 1. There are six rigid parts 
in the frame,  but parts $C$ and $D$ can be perceived as links (constraints) which
simplifies the analysis and is therefore recommended.
Internal reaction $E$ is further specified as the force between the left 
half and the right half of the frame. If it were not, several perceptions would be
possible since there are four parts connected by hinge $E$. Also an ad hoc sign
convention need be adopted for the reaction. We perceive force $E$ as acting upon
the left half of the frame.
\\ \\
\insFigUp{sa-exer5-hw.pdf}{0.8\textwidth}

\newpage
HW4 Solution:
\\
The influence lines of all variables required will have two straight parts and
will have zero points above supports $A$ and $B$. This assertion follows from the
PVd and from the fact that all variables have zero values when the unit force stands
above those supports. With one exception -- reaction $A_y$ is one when the unit force
stands above it. The values at two other locations are solved for in the two load
cases shown in the right half of the sketch. In both load cases the vertical
components of reactions $A$ and $B$ follow from the equil. conditions 
of the whole structure and afterwards the horizontal component of $B$ from the
conditions of the right half of the structure. Then the forces $C$ and $A-E$ are
determined from the equil. conditions of point $A$. They are shown in graphic 
vector schemes inlayed below both frame sketches. The fact is used that
upon the quarter circle part A-E just two forces act at the hinges. The two 
forces thus have the direction $A-E$ and equal each other. \emph{Part $A-E$ acts
as a link externally} although moments exist in it internally. The magnitudes of 
$C$ and $A-E$ forces were determined numerically, based on the vector schemes. 
Once the force $A-E$ is known, it is easy to compute the internal forces at
cross-section $p$. The arm of the force with respect to section $p$ is 2.93,
see the upper load case.
\\
\insFigUp{sa-exer5-hw-sol.pdf}{\textwidth}
The extremes of the variables are in the table:
\[\begin{array}{ccccccccc}
&A_x&A_y&E_x&E_y&C&M_p&V_p&N_p\\
max&5&20&5&5&0&10.3&0&3.03\\
min&-5&-2.5&-5&-5&-14.1&-50.4&0&-17.1
\end{array}
\]


 

\end{document}
