#sa_exer8 exercises 8th week
#ex1, princ. stresses, check shear=0 at princ. directions:
sig=[8,2,-6;2,-1,2;-6,2,4]
[princ_dir,princ_sig]=eig(sig)
n=princ_dir(:,1)
f=sig*n
sig_n=f'*n
t=f-sig_n*n
#ex2, Mises condition: ********
printf("fillet of a welded joint \n");
#sig=[-150,80,80;80,0,0;80,0,0]
#sig=[-150,80,80;80,0,20;80,20,0]
sig=[-150,80,80;80,0,80;80,80,0]
sq2=sqrt(2)/2;
n=[0;-sq2;sq2;]
f=sig*n
sig_n=f'*n
t=f-sig_n*n
[princ_dir,princ_sig]=eig(sig)
sig1=princ_sig(3,3)
sig2=princ_sig(2,2)
sig3=princ_sig(1,1)
sigM=sqrt((sig1-sig2)^2+(sig1-sig3)^2+(sig2-sig3)^2)*sq2
#ex3, Mises in a I beam******
V=120;M=100;f_y=200000;
J=0.005*0.180^3/12+2*0.01*0.2*0.095^2
S=0.01*0.2*0.095
sigtop=M*0.1/J
signeck=M*0.09/J
tauneck=S*V/J/0.005
sigM=sqrt(signeck^2+3*tauneck^2)
#hw8 Mises condition in a steel sheet in biaxial stress
sig=[100,180;180,100]
[princ_dir,princ_sig]=eig(sig)
sig1=princ_sig(2,2)
sig2=0.
sig3=princ_sig(1,1)
sigM=sqrt((sig1-sig2)^2+(sig1-sig3)^2+(sig2-sig3)^2)*sq2
