# example on M-C condition
sig=[-10,8;8,-2]
tan2a=2*8/(-10+2)
alpha=atan(tan2a)*180/pi/2
[vecs,pstr]=eig(sig)
maxshear= (-pstr(1,1)+pstr(2,2))/2
# M-C condition:
0.5*(pstr(2,2)-pstr(1,1))+0.5*(pstr(2,2)+pstr(1,1))*sin(pi/6)-6*cos(pi/6)
#Mises:
I2d=((2.94+14.94)^2+(2.94+14.94)^2+(-10+14.94)^2)/6
sqrt(I2d*3)
#hw9:
printf("hw9 ***\n");
sig=[-35,30;30,-35]
[vecs,pstr]=eig(sig)
# matlab orders the eigenvalues and eigenmodes in reverse
# order greatest last.
slope=vecs(2,2)/vecs(1,2)
sigmaxx=sig(1,1);sigmayy=sig(2,2);sigmaxy=sig(1,2);
sigma_1=pstr(2,2);sigma_2=pstr(1,1);
if (sigma_2>sigma_1)
 sigma_1=sigma_2
 sigma_2=pstr(2,2)
 slope=vecs(2,1)/vecs(1,1)
endif
 s=2*sigmaxy/(sigma_1-sigma_2)
 t= 2*sigmaxy/(sigmaxx-sigmayy)
 a=atan(t)
if (s>0) 
 if (t>0) al2=a
 else al2=a+180
 endif
else 
 if (t>0) 
  al2=a-180
 else 
  al2=a
 endif
endif
alpha=al2/2
slope=tan(alpha)
# Mohr Coulomb after the shear stress added:
#MCcrit=0.5*(sigma_1+200)+0.5*(sigma_1-200)*sin(pi/6)-30*cos(pi/6)
# the x-z, y-z before the shear stress sig_xy occurred
MCcrit=0.5*(-35+200)+0.5*(35-200)*sin(pi/6)-30*cos(pi/6)
# strain
printf("strain - displacement give \n");
eps=[0.001,0.001;0.001,-0.001]
[dirs,preps]=eig(eps)
