# Exercise 1 in sec.3 of the Notes:
eps=[0.001,0.00025;0.00025,0.0005]
[dirs,princ]=eig(eps)
n=[-1;1]/sqrt(2)
# determining 2*alpha:
epsxy=eps(1,2);eps_1=princ(2,2);eps_2=princ(1,1);epsxx=eps(1,1);
epsyy=eps(2,2);
s=2*epsxy/(eps_1-eps_2)
t=2*epsxy/(epsxx-epsyy)
a=atan(t)
if (s>0) 
 if (t>0) al2=a
 else al2=a+180
 endif
else 
 if (t>0) 
  al2=a-180
 else 
  al2=a
 endif
endif
alpha=al2/2*180/pi
epsn=n'*eps*n
#the strain in a vector:
epsvec=[eps(1,1);eps(2,2);2*eps(1,2)]
# the stress in material with
D=[1,0,0;0,1,0;0,0,0.5]
#is
sigvec=D*epsvec
# strain energy
w=0.5*sigvec'*epsvec
#
# ex3, hw10:
printf("***stress by 60 deg rosette, exer. e1=0, HW e1=-0.00003 \n");
# ex3:
# gauges 60 degs, the readings, e1 in x dir., e2 120 deg, e3 60 deg 
e1=0
e2=0.00003
e3=-0.00003
# hw10:
# gauges 60 degs, the readings, e1 in x dir., e2 120 deg, e3 60 deg 
 e1=-0.00003
#
exx=e1;
E=30000;nu=0.15
c2=-0.5;s2=sqrt(3)/2;c2^2+s2^2
c3=0.5;s3=sqrt(3)/2;
M=[s2^2,2*c2*s2;s3^2,2*s3*c3]
rs=[e2-e1*c2^2;e3-e1*c3^2]
eps=M\rs
eyy=eps(1);exy=eps(2);ezz=0;
epsmat=[exx,exy;exy,eyy]
#check
e2check=[c2,s2]*epsmat*[c2;s2]
e3check=[c3,s3]*epsmat*[c3;s3]
D=E/((1+nu)*(1-2*nu))*[1-nu,nu,0;nu,1-nu,0;0,0,(1-2*nu)/2]
epsvec=[exx;eyy;2*exy]
sigvec=D*epsvec
sigmat=[sigvec(1),sigvec(3);sigvec(3),sigvec(2)]
[pridir,pristr]=eig(sigmat)
# sig_zz:
sigzz=D(2,1)*exx+D(2,1)*eyy+D(2,2)*ezz
# stress assuming plane stress conditions
D=E/(1-nu^2)*[1,nu,0;nu,1,0;0,0,(1-nu)/2]
sigvec=D*epsvec
# eps_zz:
ezz=-(exx*nu+eyy*nu)/(1-nu)
# or
ezz=(-sigvec(1)*nu-sigvec(2)*nu+0*1)/E





