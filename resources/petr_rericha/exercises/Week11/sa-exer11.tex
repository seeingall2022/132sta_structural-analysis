\documentclass[12pt]{article}
\usepackage{graphics,../my,../comb}
\usepackage{makeidx}
\graphicspath{{figures/}}
%\newlength{\figwidth}
%\newcommand{\insinFig}[3]
%   {\makebox[0pt][l]{\raisebox{-#1\figwidth}{\hspace{#2\figwidth} #3}}}
%\newcommand{\insFig}[2]
%   {\makebox[#2][l]{\resizebox{#2}{!}
%   {\rotatebox{-90}{\includegraphics{#1}}}}}

\oddsidemargin 0mm

\renewcommand{\sketch}{Sketch}
\renewcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{STA course, exercise and homework 11, the finite element method}
\author{Petr \v Re\v richa }
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt
The default units used in numerical examples are $m$, $kN$, $kNm$, $kPa$.
 We thus do not indicate units in examples as a rule.
This is valid for the whole course. In design practice the units must be given
at least for the results!
\\ \\
The exercise complements sections 6.1 - 6.3 of the lecture notes
'FEM method'
on the course web page, just 'Notes' forthwith. They together
should substitute classes of the present 11th week of the semester.
Questions concerning the stuff, address, please, to\\
'petr.rericha@fsv.cvut.cz'.\\ 
The purpose of the eleventh week's exercise is to learn the basic
principles and techniques of the finite element method. 
The method is rather universal and is nowadays used for all types
of the building and other structures. In this short introduction
the 'classic' application to 3D, 2D and 1D elastic continuum is
demonstrated and exercised. 
It is recommended to try to solve each of the examples step by step
on your own first
and only when at loss at some point of the solution, resort to the text.
This is how I tend to teach in contact classes. In this contactless
form it requires more selfcontrol on your part, of course. 
\\
HW 11 should be delivered at the same address by Tuesday, December 8th,
 2pm.

\subsubsection{Thin bar - 1D continuum}
The uniaxial stress in a thin bar can be almost always solved exactly and
approximate methods like the FEM are not necessary. On the other hand,
the FEM solution of such a simple problem exposes the basic steps,
'the bare bone' 
of the method. A prismatic bar of length $l$ and cross-section area
$A$ is loaded by an axial uniform load $f$. The stress is uniaxial,
the strain is not (unless $\nu=0$) but the lateral strain components
do not affect the solution. Scalar variables $\sigma$ and $\varepsilon$ 
are sufficient to specify the stress/strain state, the constitutive
equation is $\sigma=E\varepsilon$, the geometric equation 
$\varepsilon=\dd u /\dd x$. Exact solution of the
example is determined by the equilibrium condition
\[ {\dd \sigma \over \dd x}+f/A=0,~~~\Rightarrow~~
  u={fx\over 2AE}(l-x)\]
and boundary conditions $u(0)=u(l)=0$.
The stress in the bar $\sigma=E\dd u/\dd x=f/A(l/2-x)$.
\\
\insFigUp{sa-exer11-ex1a.pdf}{0.8\textwidth}
\\
The weak form of the equilibrium condition derived by the principle
of virtual work reads (see eqs (44), (46) in the Notes).
\[\int_0^l (\sigma \delta \varepsilon -f \delta u)\dd x=0,~~~
\forall~\delta u~~ {\rm meeting~condition}~~ u(0)=0\]
Several methods can be used to find an approximate solution using
the weak form, among them the Ritz and Galerkin methods are the most 
popular.
 
Up to this point the FEM did not enter the game. Now it is used to
discretize the continuum. The bar is divided in several elements
connected by nodes. In our school example three elements and four
nodes are used. The actual $u$ is approximated by a linear combination
of shape functions. The shape functions can be
arbitrary but for one condition - they should be continuous. 
In this example the simplest
possible shape functions are used, linear within each element,
with magnitudes 1 at nodes.  The set
of shape functions $h_1(x) - h_4(x)$ is shown in the figure below.
 Their
linear combination $\bar u(x) = \sum_i r_i h_i(x)$ with variable
node displacements $r_i$, the Degrees of Freedom (DOF(s)),
covers all possible continuous functions
linear between nodes. This is the present special case of 
approximations~(47) or~(51) or~(58) in the Notes.  
Note that each function is non-zero only on the neighbor elements
of a single node. This property:
\begin{itemize}
\item implies that the structure stiffness
matrix $[K]$ has nonzero entries in a relatively narrow band around
the main diagonal,
\item  features the FEM in general and distinguishes it from other
implementations of the Ritz and Galerkin methods.
\item facilitates the construction of the shape functions since they
can be developed on individual elements.
\end{itemize}  
Our shape functions on an element $i,j$
are shown in the right sketch of the figure. 
\\
\insFigUp{sa-exer11-ex1b.pdf}{0.8\textwidth}
\\
The matrix of the shape functions (58) in the Notes reduces to a
row:
\[ [h]^T=\{h_1,h_2,h_3,h_4\}\]
and operator $[\partial]$ simplifies to $\dd /\dd x$
Matrix $[B]$ and the integral in eq. (61) in the Notes are perfomed
element by element. The contributions to $[K]$ from individual elements
are the element stiffness matrices $[K]^{i,j}$. All shape functions are
linear, their derivatives are thus constants and the integration
simplifies to multiplication by factors $l_{i,j}AE$.
\\
element 1,2\\
$[B]^T=[-1/l_{1,2},1/l_{1,2}],~~~[K]^{1,2}=[-1/l_{1,2},1/l_{1,2}]^T AE 
[-1/l_{1,2},1/l_{1,2}]l_{1,2}=
AE/l_{1,2}\left[\begin{array}{cc}1&-1\\-1&1 \end{array}\right] $
The stiffness matrices of all elements obviously can be written down
in a single formula
\[[K]^{i,j}=AE/l_{i,j}\left[\begin{array}{cc}1&-1\\-1&1 \end{array}\right]\]
Recall that 
$\{\delta r_i,\delta r_j\}[K]^{i,j}
  \left\{\begin{array}{c}r_i\\r_j\end{array}\right\}$
is the contribution of the element to the integral (61) in the Notes,
and $[K]^{i,j}$ is the contribution of the element to the global stiffness
matrix $[K]$. The assembly of $[K]$ is already known from the
matrix displacement method for planar beams and gridworks.
Contributions of the elementa will be allocated in the global stiffness
matrix including the DOFs in the natural order $r_1,~r_2~...$.

The contribution of element $i,j$ to $\{F\}$ in the integral (62) 
in the Notes is the following:
\[ \{F\}^{i,j}= \int \left\{\begin{array}{c}h_1\\h_2
\end{array}\right\}\dd x=
l_{i,j}/2\left\{\begin{array}{c}1\\1\end{array}\right\} \]
$f=1$ is used in the FEM solution to simplify the development.
This is a common trick in real analyses since for any load
intensity the results are multiples of the solution with unit intensity.
The assembly of the global $\{F\}$ again is the same as in the
matrix displacement method.

Up to this point variable element lengths are admitted. The actual
element mesh for the bar has elements of equal lengths $l_{i,j}=l/3$.
The global matrices are then
\[[K]=3AE/l\left[\begin{array}{cccc}1&-1&0&0\\-1&2&-1&0\\0&-1&2&-1\\0&0&-1&1
 \end{array}\right],~~~
\{F\}= l/3\left\{\begin{array}{c}0.5\\1\\1\\0.5\end{array}\right\} \]
Note the banded shape of the stiffness matrix.

The supports (boundary conditions) are applied next. Nodes 1 and 4
are fixed, $r_1=r_2=0$. The first and fourth rows and columns can
be omitted in the above matrices and the final equilibrium equation
reads
\[ 3AE/l\left[\begin{array}{cc}2&-1\\-1&2\end{array}\right]
\left\{\begin{array}{c}r_2\\r_3\end{array}\right\}=
l/3\left\{\begin{array}{c}1\\1\end{array}\right\},~~~\Rightarrow~~~
\left\{\begin{array}{c}r_2\\r_3\end{array}\right\}=
l^2/(9AE)\left\{\begin{array}{c}1\\1\end{array}\right\} \]
The back substitution of the node displacements also is familiar
from the matrix displacement method but in case of the FEM we
(as a rule) do not want the node forces but the stresses in the
elements:
\[ \sigma_{1,2}=3E\{-1/l, 1/l\}l^2/(9AE)
\left\{\begin{array}{c}0\\1\end{array}\right\}=l/(3A) \]
\[ \sigma_{2,3}=3E\{-1/l, 1/l\}l^2/(9AE)
\left\{\begin{array}{c}1\\1\end{array}\right\}=0 \]
\[ \sigma_{2,3}=3E\{-1/l, 1/l\}l^2/(9AE)
\left\{\begin{array}{c}1\\0\end{array}\right\}=-l/(3A) \]
The exact (dashed line) and the FEM solution stress variations along the
length of the bar are plotted in the second figure of the section.
It is easy to imagine, how the FEM solution will look like when
the number of elements is increased. The stepwise diagram of the
stress by the FEM will be closer to the exact solution. The FEM solution
apparently converges to the exact one for increasing number of elements.

Boundary conditions have been applied after the $[K]$ and $\{F\}$ 
matrices were set up with all DOFs included. Recall that in the solution
of planar frames and gridworks an alternative was preferred when
the boundary conditions were applied on the element level already.
This is possible and often utilized in the FEM, too.
The $[K]$ and $\{F\}$ are then set up without DOFs $r_1$ and $r_4$.
Element matrices $[K]^{1,2}$ and $[K]^{3,4}$ are stripped of the respective
DOFs so that just 1x1 matrices remain. The same applies to $\{F\}^{1,2}$
and $\{F\}^{3,4}$. The final equilibrium equation is the same, of course.
\\ \\
HW11:\\
Solve the bar with the right end free and using just two elements
of lengths $l/2$. Apply boundary conditions on the element level.
 Everything else remains the same. The bar is
statically determinate and the equilibrum conditions alone
yield exact solution $\sigma= f(l-x)$.

\newpage
HW11 Solution:\\
Two DOFs $r_2$ and $r_3$ remain when the boundary condition $r_1=0$ 
is applied. $[B]^{1,2}$ and $[K]^{1,2}$ matrices reduce to single
numbers $1/l_{1,2}$ and $AE/l_{1,2}$ respectively.
\[  [B]^{2,3}=1/l_{2,3}[-1,1],~~~[K]^{2,3}=AE/l_{2,3}
\left[\begin{array}{cc}1&-1\\-1&1\end{array}\right] \]
With $l_{1,2}=l_{2,3}=l/2$ the global stiffness and load matrices
and the node displacements are
\[ [K]=2AE/l\left[\begin{array}{cc}2&-1\\-1&1\end{array}\right],~~~
   \{F\}= fl/2\left\{\begin{array}{c}1\\0.5\end{array}\right\},~~~
   \{r\}={fl^2 \over 4AE}\left\{\begin{array}{c}1.5\\2\end{array}\right\}.\]
\[\sigma_{1,2}=AE/l_{1,2}{fl_{1,2}^2 \over AE}\cdot 1.5= 0.75fl,~~~
 \sigma_{2,3}=AE[B]^{2,3}\{r\}=0.25fl \]
\\
\insFigUp{sa-exer11-hw.pdf}{0.8\textwidth}




  
 

\end{document}
