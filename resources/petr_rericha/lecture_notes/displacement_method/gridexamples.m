1;
function kii=kgenii(k,tor,l)
  kii=[6*k/l^2,0,-3*k/l;0,tor,0;-3*k/l,0,2*k];
endfunction
function kjj=kgenjj(k,tor,l)
  kjj=[6*k/l^2,0,3*k/l;0,tor,0;3*k/l,0,2*k];
endfunction
function kij=kgenij(k,tor,l)
# actually this is kijij. kijji=kijij^T= kgenij()^T!!!
  kij=[-6*k/l^2,0,-3*k/l;0,-tor,0;3*k/l,0,k];
endfunction
function kiipinj=kgeniipinj(k,tor,l)
  kiipinj=[1.5*k/l^2,0,-1.5*k/l;0,0,0;-1.5*k/l,0,1.5*k];
endfunction
function tmat=t(cal,sal)
 tmat=[1,0,0;0,cal,sal;0,-sal,cal];
endfunction
#
# gridwork example 1, manuscript,two beams, 1,3 and 3,2 nodes
# 1,2 clamped, 3,free
#                                      2 --
#                                     /   |
#                                    /   4|
#                        1----------3  ----
#                        |     5    | 3|
#                        |----------|--|
#E=39.1,J=0.0256 (0.4x0.4m),EJ=1,l=5,G=E/2,Jt=2J,tor=GJy/l=1
#
k13loc=kgenjj(0.4,0.2,5);
t23=t(3/5,4/5);
t23T=t23'
k32loc=kgenii(0.4,0.2,5);
k32glo=t23T*k32loc*t23
kglo= k13loc+k32glo;
f=[-1;0;0];
u=kglo\f
#u=cgs(kglo,f)
S13=k13loc*u
S32=k32loc*t23*u
#
#example 2, homework, manuscript, the same structure as in example1
#except that joint 1 i pin supported. The stiffness matrix of example 1
#is extended by two rows and columns to include the two rotations
#at joint 1. The DOF order is (phi1x,phi1y,w3,phi3x,phi3y)
#
printf("Joint 1 pin supported:");
k=0.4;tor=0.2;l=5;
extendcolumns=[0,-tor,0;3*k/l,0,k]
k13intermed=vertcat(extendcolumns,k13loc)
extendrows=[tor,0;0,2*k;0,3*k/l;-tor,0;0,k]
k13eloc=horzcat(extendrows,k13intermed)
extendcolumns=[0,0,0;0,0,0]
k32eglo=horzcat([0,0;0,0;0,0;0,0;0,0],vertcat([0,0,0;0,0,0],k32glo))
keglo=k13eloc+k32eglo
fe=vertcat([0;0],f)
ue=keglo\fe
#ue=cgs(keglo,fe)
S13e=k13eloc*ue
uecut=[ue(3);ue(4);ue(5)]
S32=k32loc*t23*uecut
#
#Example 3, lecture notes p.56(manual solution), rectangular grid 
#l13=4,l23=2 join 3 pin supported, k12=0.5, k23=1,
#tor12=0.125,tor23=0;
#Three variants can be computed:
#1. variant - clamped end 1, pin support at 3. This variant can be 
#simplified in manual solution by elimination of rotation
#phi_y, since M2,3,y=M2,1,y=0, see manuscript.
#k23loc=kgeniipinj[1,0,2];
#f={5/8*2,1/8*2^2,0}
#2. variant - both ends clamped
#Modified for homework, joint 3 clamped, then tor23=0.25,
#k23loc=kgenii[1,0.25,2]
#f={1/2*2,1/12*2^2,0}
#3. variant - both ends clamped and load by concentrated force at joint 2.
#k23loc=kgenii[1,0.25,2]
#f={1,0,0}
#
printf("Rectangular grid, manual sol. in notes, ends 1,3 clamped\n");
k12=kgenjj(0.5,0.125,4)
k23loc=kgenii(1,0.25,2);
t23=t(0,1);
k23glo=t23'*k23loc*t23
kglo=k12+k23glo
#f=[5/8*2;1/8*2^2;0]
f=[1/2*2;1/12*2^2;0]
#f=[1;0;0];
u=-kglo\f
#u=cgs(kglo,-f)
S21=k12*u
S12=kgenij(0.5,0.125,4)*u
S23d=k23loc*t23*u
S23=S23d+t23*f
#
#Exam test 18/12/06, 16/3/07, 30/1/08,4/2/09
#
printf("Exam test \n");
kb=20;tors=8;len=10;
k14=kgenjj(kb,tors,len)
t24=t(0.5,0.867)
k24=t24'*k14*t24
t34=t(-0.5,0.867)
k34=t34'*k14*t34
Ktot=k14+k24+k34
#
#Example 4 homework, two beams, 1,3 and 3,2, both 5m long, beam 1,3 
#in global x axis, beam 3,2 at cos=3/5, sin=4/5, ends 1 and 2 clamped, 
#joint 3 pin supported.
#Beam 3,2 loaded by a uniform load 1. Posed 6/11/09.
#The solution obtained on structure without pin support at joint 3 
#(in order to use the stifness matrices kgen functions).
#Reaction force is iteratively added to the load vector F32l until 
#the z (first) component of {u} approximately vanishes.
#
#
#                                       2
#                                      *
#                                     *
#                                    *
#                                   *
#                   1  *  *  *  *  3

printf("Example 4, 1 and 2 clamped, 3 pin support\n");
printf("Solution with 3 DOF, reaction Z=2.187 determined by iteration \n");
# beam 1,3
k31=kgenjj(0.4,0.2,5)
# beam 3,2
k32l=kgenii(0.4,0.2,5)
trmat=t(3/5,4/5)
k32g=trmat'*k32l*trmat
kglob=k31+k32g
# reaction at 3 determined by iteration
f32l=[2.5-2.187;0;-25/12]
f32g=trmat'*f32l
u=-kglob\f32g
#u=cgs(kglob,-f32g)
S31=k31*u
S32=k32l*trmat*u
printf("Solution with 2 DOF \n");
kglobred=[kglob(2,2),kglob(2,3);kglob(3,2),kglob(3,3)]
fl=[0;0;-25/12]
fg=trmat'*fl
fgred=[fg(2);fg(3)]
ured=-kglobred\fgred
u=[0;ured(1);ured(2)]
S31=k31*u
uloc=trmat*u
S32=k32l*uloc
# the reaction without fixed end force:
printf("the reaction without fixed end force:\n");
res= kglob*u
#
#    HW7 15/11/11
#    1------------2         ---
#                  \
#                   \        4        EJ=10, GJt=10
#                    \
#                     \
#                      3    ---
#    |      4     | 4  |
#                    
#   1 and 3  clamped, 2 pin supported, beam 2,3 loaded by uniform 1
#
 printf("HW7 15/11/11  \n ");
 k12=2*10/4
 g12=10/4
 sq2=sqrt(2)
 k23=2*10/4/sq2
 g23=10/4/sq2
 K12=kgenjj(k12,g12,4)
 k23l=kgenii(k23,g23,4*sq2)
 tmat=t(sq2/2,-sq2/2)
 k23g=tmat'*k23l*tmat
 k=K12+k23g
 kred=[k(2,2),k(2,3);k(3,2),k(3,3)]
 fl=[4*sqrt(2)/2;0;-1/12*4^2*2]
 f=tmat'*fl
 fred=[f(2);f(3)]
 ured=-kred\fred
 u=[0;ured(1);ured(2)]
 s21=K12*u
 ul23=tmat*u
 s23d= k23l*ul23
 s23= s23d+fl
#
#  exam task 10/1/2012:
#
#                           3  --
#    load  f=1 on 1,3     / |   |
#   EJ=1                 /  |   |
#   GJt=1               /   |   2
#                      /    |   |
#                     1     2  --
#                     |--2--|   
printf("exam task set 4, preface - solve for the displacements: \n");
EJ=1; GJt=1;f=1;
l13=2*sqrt(2); l23=2;
k13=2*EJ/l13
k23=2*EJ/l23
g13=GJt/l13
g23=GJt/l23
k13l=kgenjj(k13,g13,l13)
k23l=kgenjj(k23,g23,l23)
cands=1/sqrt(2)
trm13=t(cands,cands)
trm23=t(0,1)
k13g=trm13'*k13l*trm13
k23g=trm23'*k23l*trm23
kg=k13g+k23g
f31l=[f*l13/2;0;f*l13^2/12]
f31g=trm13'*f31l
u3=-kg\f31g
# the exam task in set 4 starts here with displacements given
# from the above, just k13l, trm13, k23l, trm23 are used
# transforming u to local
printf("exam task set 4, the task itself, \n");
printf("just k13l, trm13, k23l, trm23 are used from the above. \n");
printf("transform u to local coord.: \n");
u31l=trm13*u3
s31l=k13l*u31l
s31ltot= s31l+f31l
k1331=kgenij(k13,g13,l13)
s13l=k1331*u31l
f13l=[f*l13/2;0;-f*l13^2/12]
s13ltot= s13l+f13l
u32l=trm23*u3
s32l=k23l*u32l
# transforming s to local
printf("transform s to local coord.: \n");
s31g=k13g*u3
s31l=trm13*s31g
#
#  test example 19/12/2012
#   beam at 45deg. 4:4m, pin force F=1 at center, given
#  end displacements
printf("test example 19/12/2012, beam at 45deg. 4:4m, pin force F=1 at center \n");
k=1;g=1;F=1;c=sqrt(2)/2;s=c;
l=4/c
u1g=[-0.1;0.05;0.05]
u2g=[0;0.05;-0.05]
trm=t(c,s)
u1l=trm*u1g
u2l=trm*u2g
k11=kgenii(k,g,l)
k12=kgenij(k,g,l)
k22=kgenjj(k,g,l)
s12=k11*u1l+k12*u2l
s21=k22*u2l+k12'*u1l
printf("beam with imposed end displ. and central point force\n");
u4=[-0.03;-0.006;-0.008];u1=[0.02;0.01;-0.02];
k=4;g=1;
tm=t(0.8,-0.6)
F4=[0.05;0;-0.0625]
k44=kgenii(4,1,5)
k41=kgenij(4,1,5)
k11=kgenjj(4,1,5)
s41=k44*tm*u4+k41*tm*u1+F4
printf("three clamped beams 1,4;2,4;3,4 un. load on 1,4\n");
EJ=100;GJ=80;
l14=10;l24=10;l34=10;
k14=2*EJ/l14
g14=GJ/l14
k24=k34=2*EJ/l24
g24=g34=GJ/l24
k1444=kgenjj(k14,g14,l14)
k2444l=kgenjj(k24,g24,l24)
tr24=t(0.5,0.86603)
k2444g=tr24'*k2444l*tr24
tr34=t(-0.5,0.86603)
k3444g=tr34'*k2444l*tr34
k=k1444+k2444g+k3444g
F=[5;0;8.33]
u=-k\F
S41= k1444*u
S41=S41+F
S42= k2444l*tr24*u
S43= k2444l*tr34*u
#****************************
# test example set 2
printf("test example set 2\n");
EJ=5;GJ=2.5;l=10;
k=2*EJ/l
g=GJ/l
k33l=kgenjj(k,g,l)
cs=sqrt(2)/2
t13=t(cs,cs)
k33g=t13'*k33l*t13
t23=t(-cs,cs)
k2333g=t23'*k33l*t23
k=k33g+k2333g
f31l=[0.5;0;1.25]
f31g=t13'*f31l
