# Set up material compliance matrix for plane stress matrix Cm and
# stiffness matrix for plane strain Dm. Invert them to get the matrices
# for reverse evaluations.
# For given stress matrix from the example with stress at the soil body
# surface compute strain vectors assuming plane stress and plane strain
# conditions with 
E=10000; ny=0.3;
Sigma=[0.25,-1.3;-1.3,-1.25]
t=[0.5,sqrt(3)/2;-sqrt(3)/2,0.5]
printf("stress matrix in n-t coord.\n");
Sigmarot=t*Sigma*t'
Sigmarotvec=[Sigmarot(1,1);Sigmarot(2,2);Sigmarot(1,2)]
#
# compliance matrix in plain stress (extract from 3D compliance)
function cm=compmat(E,ny)
 C=1/E; cm=[C,-ny*C,0;-ny*C,C,0;0,0,2*(1+ny)*C];
endfunction
# stiffness matrix in plain strain (extract from 3D stiffness)
function dm=stifmat(E,ny)
 fac=E/(1+ny)/(1-2*ny);
 dm=[fac*(1-ny),fac*ny,0;fac*ny,fac*(1-ny),0;0,0,fac*(1-2*ny)/2];
endfunction
# stiffness matrix in plain stress
function dm=stifmatplanestress(E,ny)
 E
 ny
 fac=E/(1-ny^2)
 dm=[fac,fac*ny,0;fac*ny,fac,0;0,0,fac*(1-ny)/2];
endfunction
# 
printf("Strain vector in plane stress conditions:\n")
Cm=compmat(E,ny)
epsplanestress=Cm*Sigmarotvec
#
printf("Strain vector in plane strain conditions:\n")
DmInv=inv(stifmat(E,ny))
epsplanestrain=DmInv*Sigmarotvec
#
# HW11 ********************************************
printf("hw11********************************\n");
printf("given epsxx=0.5, epsyy=-0.2, eps45=0, E=30GPa, nu=0.15\n");
#  given incomplete strain tensor eps 
#   0.5 exy
#   exy -0.2
# determine exy so that extension at 45deg is 0.
# functions defined below admit a nonzero extension at any angle!!
#
global exx=0.5 eyy=-0.2;
function ealpha=extalpha(alpha,exy,subtract)
# global exx,eyy: extensions in x , y directions
# alpha: angle, exy: shear strain xy
# output: extension at alpha angle from x, subtract subtracted
 global exx eyy;
 stten=[exx,exy;exy,eyy];
 n=[cos(alpha/180*pi);sin(alpha/180*pi)];
 ealpha=n'*stten*n- subtract;
endfunction
#**************
printf("check the functions:\n");
# This is a check of the functions. We specify exy=0.1, compute e45 and
# using fzero(), compute back exy:
# extension at 45deg with exy=0.2:
global e45= extalpha(45,0.2,0)
e45
# since fzero() admits just univariate function handle, we must
# define a function that returns a zero value when extension at 45deg
# is e45. For that purpose, the parameter subtract of extalpha() is used.
function e45sub=ext45sub(exy)
 global e45;
 e45sub=extalpha(45,exy,e45);
endfunction
#now, solve back for exy when the extension at 45deg is e45:
[exy,fval,info]=fzero(@ext45sub,0)
# or, when no information on the iteration is required:
exy=fzero(@ext45sub,0)
#**************
printf("homework:\n");
# The homework alone, with e45=0:
e45=0;
exy=fzero(@ext45sub,0)
# for this strain, compute stress in plane strain conditions
stten=[exx,exy;exy,eyy]/10000
stvec=[exx;eyy;2*exy]/10000
printf("stress in plane strain cond. [MPa]\n");
E=30000;ny=0.15;
stmat=stifmat(E,ny)
sigplanestrain=stmat*stvec
sigzz=(sigplanestrain(1)+sigplanestrain(2))*ny
# for this strain, compute stress in plane stress conditions
printf("stress in plane stress cond. [MPa]\n");
stmat=inv(compmat(E,ny))
sigplanestress=stmat*stvec
# epszz
printf("Lateral extension epszz:\n");
epszz=-ny/(1-ny)*(exx+eyy)/10000
#
#***************************************************
# exam task 19/12/2012: given 45deg rosette readings 
printf("\n\nexam task 19/12/2012: given 45deg rosette readings \n");
epsxx= 0.002
epsyy= -0.001
eps45= 0
#  on thin steel sheet with 
E=200
ny=0.3
# compute principal streses and check Mises with fy=400MPa
fy=0.4
epsxy= -epsxx*0.5 - epsyy*0.5
eps=[epsxx;epsyy;2*epsxy]
dscr= sqrt(((epsxx-epsyy)/2)^2+epsxy^2)
eps1=(epsxx+epsyy)/2+dscr
eps2=(epsxx+epsyy)/2-dscr
printf("stiff. matrix in plane stress:\n");
#dmtx=inv(compmat(E,nu))
dmtx=stifmatplanestress(E,ny)
sig=dmtx*eps
sigi= dmtx*[eps1;eps2;0]
I2deviator=((sigi(1)-0)^2+(sigi(1)-sigi(2))^2+(sigi(2)-0)^2)/6
printf("fy^2/3= %f \n",fy^2/3);
