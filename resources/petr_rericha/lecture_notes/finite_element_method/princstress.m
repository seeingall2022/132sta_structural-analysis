#
# SA - principal stress examples 
#
# compute the principal stresses and their directions in 3D
# seems the vectors returned by eig() are normed
# and ordered in columns of the princdir matrix
#
strten=[-30,25,0;25,0,25;0,25,0]
[princdir,princstr]=eig(strten)
#
# compute the traction, normal and tangential stress of stress tensor
#
strten=[8,2,-6;2,-1,2;-6,2,4]
n=[0.333;0.666;0.666]
f=strten*n
sn= f'*n
t=f-sn*n
tlength=sqrt(t'*t)
[princdir,princstr]=eig(strten)
#
# Test set I : 
strten=[200,60,-40;60,100,-120;-40,-120,-200]
f=strten*n
sn=f'*n
t=f-sn*n
tlength=sqrt(t'*t)
printf("Mises condition, fy=400: \n");
I2dev=((strten(1,1)-strten(2,2))^2+(strten(1,1)-strten(3,3))^2
 +(strten(2,2)-strten(3,3))^2)/6 +strten(1,2)^2+strten(1,3)^2+strten(2,3)^2
effstress=sqrt(3*I2dev)
printf("material fails \n");
#
#examples from Exeter
#
printf("Exeter examples:\n");
strten=[10,5,-10;5,20,-15;-10,-15,-10]
n=[0.5;1/sqrt(2);0.5]
f=strten*n
sn=f'*n
t=f-sn*n
tlength=sqrt(t'*t)
printf("modified n: \n")
n=[1;1;1]/sqrt(3)
f=strten*n
sn=f'*n
t=f-sn*n
tlength=sqrt(t'*t)
[princdir,princstr]=eig(strten)
#
printf("********************************\n");
# The other Exeter example:
# hw 10 15/12/15 modified, sxy= -150 instead of -1500
printf("The other Exeter ex.:\n")
sxz=500*sqrt(2);
strten=[500,-1500,sxz;-1500,500,-sxz;sxz,-sxz,2000]
#strten=[500,-150,sxz;-150,500,-sxz;sxz,-sxz,200]
[princdir,princstr]=eig(strten)
#
# class example 
printf("********************************\n");
printf("class example:\n");
strten=[7,0,0;0,6,-12;0,-12,-1]
fac=sqrt(3)/3;
n=[fac;fac;fac]
f=strten*n
sn= n'*f
t=f-sn*n
tlength=sqrt(t'*t)
[princdir,princstr]=eig(strten)
##

# soil (Mohr-Coulomb) example
#
printf("3D Mohr-Coulomb \n");
strten=[-1.25,0,3;0,-1.25,0;3,0,-5]
[princdir,princstr]=eig(strten)
phi=30/180*pi
sigmc=(0.5*(princstr(3,3)-princstr(1,1))+0.5*(princstr(3,3)+princstr(1,1))*sin(phi))-2*cos(phi)
#
# exam 10/1/2012:
# check Mises criterion in a steel sheet in plane stress, draw the Mohr
# circle and determine from it the principal stresses
printf("exam 10/1/2012, check Mises condition \n");
strmat=[-200,100;100,100];
strmis=sqrt(.5*((-200-100)^2+200^2+100^2)+3*100^2)
[princdir,princstr]=eig(strmat)
#
# fillet weld of a steel girder web/flange joint
printf("*****************************************\n");
printf("fillet weld of a steel girder web/flange joint \n");
sig=[-150,80,-80;80,0,-80;-80,-80,0]
[princdir,princstr]=eig(sig)
princstr(1)
#traction at flange surface
n=[0;sqrt(2)/2;-sqrt(2)/2]
f=sig*n
signormal=f'*n
#Mises stress
sig0=(princstr(1,1)+princstr(2,2)+princstr(3,3))/3
sigdev=[princstr(1,1);princstr(2,2);princstr(3,3)]-sig0*[1;1;1]
sigmises=sigdev(1)*sigdev(2)+sigdev(1)*sigdev(3)+sigdev(2)*sigdev(3)
sigmises= sqrt(-sigmises*3)
#
# sliding along a slip  at 30deg, plane stress
printf("*****************************************\n");
printf(" sliding along a slip  at 30deg\n");
sig=[-2,1;1,-6]; n=[0.5;sqrt(3)/2]
f= sig*n
signo=f'*n
t= f-n*signo
tmag= sqrt(t(1)^2+t(2)^2)
tovertmag= tmag/signo
#
# 120 deg rosette
printf("\n\n 120 deg rosette\n");
eps0=0.75;eps120=0.5625;eps60=0.6875;
# unknowns: {e}=[eyy;exy]
mat=[0.75,0.86603;0.75,-0.86603]
rs=[eps60-eps0*0.25;eps120-eps0*0.25]
e=mat\rs
[princdir,princstr]=eig([eps0,e(2);e(2),e(1)])
#
printf("given strain matrix and n, compute epsn,epsi,ni \n");
eps=[0.1,0,-0.2;0,0.2,0;-0.2,0,0.1]
n=[1;2;3]/sqrt(14)
epsn= n'*eps*n
[princdir,princstr]=eig(eps)
#
# exam example plane strain
#
printf("exam task: plane strain, given 0,90,45degs readings\n");
# prepar input data, the strain tensor when the stress is known:
sig=[-5,3,0;3,-1.25,0;0,0,-1.25]
sigv=[sig(1,1),sig(2,2),sig(1,2)]'
E= 1000;nu=0.35;c=2;phi=30;
C=(1-nu^2)/E*[1,-nu/(1-nu),0;
   -nu/(1-nu),1,0;
   0,0, 2/(1-nu)]
epsv= C*sigv
D= E/(1+nu)/(1-2*nu)*[1-nu,nu,0;
       nu,1-nu,0;
       0,0,(1-2*nu)/2]
sigvcheck= D*epsv
# reading of the 45deg gauge:
eps45= epsv(1)*0.5 +epsv(2)*0.5 + epsv(3)*0.5 # #This is the task: # check the Mohr-Coulomb crit. at a point in a soil body when # readings of the strain gauge rosette 0, 90, 45 deg. are
printf("check the Mohr-Coulomb crit., given 0,90,45deg strain readings,E,nu,c,phi \n");
eps0=epsv(1)
eps90= epsv(2)
eps45
# and the material constants are 
E=1000
nu=0.35
c=2
phi= 45/180*pi
#
# from condition epsxx*0.5+epsyy*0.5 + epsxy*0.5= eps45
#                -3.797*0.5 +1.266*0.5 +epsxy*0.5*10^3= 2.784
# we obtain
printf("from condition epsxx*0.5+epsyy*0.5 + epsxy*0.5= eps45\n");
epsv
#Next step, compute sigv
sigv = D*epsv 
sig
[princdir,princstr]=eig(sig)
