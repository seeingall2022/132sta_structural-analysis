\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{STAreview_test}[2022/03/28 Mid-term review test of course 132STA]

\LoadClass[11pt]{article}

%% Required packages
\RequirePackage[a4paper,margin=2cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[numbers]{natbib}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{pdfpages}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}

%% Bibliography setup
\bibliographystyle{elsarticle-harv}

%% Graphicx setup
\graphicspath{{figures/}}

%% Other commands
\newcommand{\noitemsep}{\itemsep=0pt}

\newcounter{SessionNumber}
\newcounter{ProblemNumber}

\renewcommand{\maketitle}[1]{%
    %% Facyhdr setup
    \pagestyle{fancy}%
    \fancyhf{}%
    \chead{Structural Analysis - 132STA | Mid-term review test~(#1)}%
    \rfoot{\thepage}%
}

\newcommand{\Problem}[2]{%
    \refstepcounter{ProblemNumber}%
    \paragraph{Problem~\arabic{ProblemNumber}~[#1 points]}%
    #2%
}

\newcommand{\References}{%
\bibliography{liter}
}

\newcommand{\Closing}{%
\vfill
\begin{center}
    \framebox{\bfseries{Good luck!}}    
\end{center}
}

\newcommand{\ResultsTable}{%
\centerline{%
 \begin{tabular}{|l||c|c|c||c|}
 \hline
\bf Name and surname & \multicolumn{3}{c||}{\bf Problem} & $\boldsymbol{\Sigma}$ \\
 & \small 1 & \small 2 & \small 3 & \\
   \hline
\hspace{103mm} & \hspace{7mm} & \hspace{7mm} & \hspace{7mm} & \hspace{7mm} \\[12mm] 
\hline
\end{tabular}
}}
